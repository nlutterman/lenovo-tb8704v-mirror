/*
 * Copyright (c) 2009-2012, The Linux Foundation. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *        * Redistributions of source code must retain the above copyright
 *            notice, this list of conditions and the following disclaimer.
 *        * Redistributions in binary form must reproduce the above copyright
 *            notice, this list of conditions and the following disclaimer in the
 *            documentation and/or other materials provided with the distribution.
 *        * Neither the name of Code Aurora nor
 *            the names of its contributors may be used to endorse or promote
 *            products derived from this software without specific prior written
 *            permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT ARE DISCLAIMED.    IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.codeaurora.fmradio;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import android.media.AudioManager;
import android.media.AudioManager.OnAudioFocusChangeListener;

import android.media.AudioSystem;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.PowerManager;
import android.os.SystemClock;
import android.os.PowerManager.WakeLock;
import android.os.RemoteException;

import android.telephony.PhoneStateListener;
// import android.telephony.MSimTelephonyManager;  // fix by lihongya 20141229
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;
import android.view.KeyEvent;
import android.os.SystemProperties;
import android.view.Gravity;

import qcom.fmradio.FmReceiver;
import qcom.fmradio.FmRxEvCallbacksAdaptor;
import qcom.fmradio.FmRxRdsData;
import qcom.fmradio.FmConfig;
import android.net.Uri;
import android.content.res.Resources;
import java.util.Date;
import java.text.SimpleDateFormat;
import android.provider.MediaStore;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import com.codeaurora.utils.A2dpDeviceStatus;
import android.media.AudioManager;
import android.content.ComponentName;
import android.os.StatFs;
import android.os.Build;
import android.os.Process;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import java.util.*;
import android.content.SharedPreferences;
import java.util.Locale;

/**
 * Provides "background" FM Radio (that uses the hardware) capabilities,
 * allowing the user to switch between activities without stopping playback.
 */
public class FMRadioService extends Service
{

   public static final int RADIO_AUDIO_DEVICE_WIRED_HEADSET = 0;
   public static final int RADIO_AUDIO_DEVICE_SPEAKER = 1;

   private static final int FMRADIOSERVICE_STATUS = 101;
   private static final String FMRADIO_DEVICE_FD_STRING = "/dev/radio0";
   private static final String LOGTAG = "FMService";//FMRadio.LOGTAG;

   private FmReceiver mReceiver;
   private BroadcastReceiver mHeadsetReceiver = null;
   private BroadcastReceiver mSdcardUnmountReceiver = null;
   private BroadcastReceiver mMusicCommandListener = null;
   private BroadcastReceiver mAirplaneModeChangeListener = null;
   // broadcastReceiver to handle event or system language changed
   private BroadcastReceiver mLocaleReceiver = null;
   private BroadcastReceiver mSleepExpiredListener = null;
   private boolean mSleepActive = false;
   private BroadcastReceiver mRecordTimeoutListener = null;
   private BroadcastReceiver mDelayedServiceStopListener = null;
   private BroadcastReceiver mAudioBecomeNoisyListener = null;
   private boolean mOverA2DP = false;
   private BroadcastReceiver mFmMediaButtonListener;
   private BroadcastReceiver mFMExternalStopReceiver = null;
   private IFMRadioServiceCallbacks mCallbacks;
   private static FmSharedPreferences mPrefs;
   private boolean mHeadsetPlugged = false;
   private boolean mInternalAntennaAvailable = false;
   private WakeLock mWakeLock;
   private int mServiceStartId = -1;
   private boolean mServiceInUse = false;
   private static boolean mMuted = false;
   private static boolean mResumeAfterCall = false;
   private static String mAudioDevice="headset";
   MediaRecorder mRecorder = null;
   MediaRecorder mA2dp = null;
   public boolean mFMOn = false;
   private boolean mFmRecordingOn = false;
   private boolean mSpeakerPhoneOn = false;
   private int mCallStatus = 0;
   private BroadcastReceiver mScreenOnOffReceiver = null;
   final Handler mHandler = new Handler();
   private boolean misAnalogModeSupported = false;
   private boolean misAnalogPathEnabled = false;
   private boolean mA2dpDisconnected = false;
   //PhoneStateListener instances corresponding to each

   private FmRxRdsData mFMRxRDSData=null;
   // interval after which we stop the service when idle
   private static final int IDLE_DELAY = 60000;
   private File mA2DPSampleFile = null;
   //Track FM playback for reenter App usecases
   private boolean mPlaybackInProgress = false;
   private boolean mStoppedOnFocusLoss = false;
   private File mSampleFile = null;
   long mSampleStart = 0;
   private long mSampleDuration = 0;

   private boolean resumeSpeaker = false;
   private boolean resumePlayback = false;

   public boolean isExiting = false;
   // Messages handled in FM Service
   private static final int FM_STOP =1;
   private static final int RESET_NOTCH_FILTER =2;
   private static final int STOPSERVICE_ONSLEEP = 3;
   private static final int STOPRECORD_ONTIMEOUT = 4;
   private static final int FOCUSCHANGE = 5;
   //Track notch filter settings
   private boolean mNotchFilterSet = false;
   public static final int STOP_SERVICE = 0;
   public static final int STOP_RECORD = 1;
   // A2dp Device Status will be queried through this class
   A2dpDeviceStatus mA2dpDeviceState = null;
   private boolean mA2dpDeviceSupportInHal = false;
   //on shutdown not to send start Intent to AudioManager
   private boolean mAppShutdown = false;
   private boolean mSingleRecordingInstanceSupported = false;
   private AudioManager mAudioManager;
   public static final long UNAVAILABLE = -1L;
   public static final long PREPARING = -2L;
   public static final long UNKNOWN_SIZE = -3L;
   public static final long LOW_STORAGE_THRESHOLD = 50000000;
   private long mStorageSpace;
   private static int region = 0;
    // caoyang_20140722 Add: High volume toast. @{
    private static final int VOLUME_TOAST = 1;
    private static final long VOLUME_TOAST_DELAYED = 20 * 3600 * 1000;// 20h
    // @}

   private static final String IOBUSY_UNVOTE = "com.android.server.CpuGovernorService.action.IOBUSY_UNVOTE";
   private PhoneStateListener[] mPhoneStateListeners;
   private static final String SLEEP_EXPIRED_ACTION = "com.quicinc.fmradio.SLEEP_EXPIRED";
   private static final String RECORD_EXPIRED_ACTION = "com.quicinc.fmradio.RECORD_TIMEOUT";
   private static final String SERVICE_DELAYED_STOP_ACTION = "com.quicinc.fmradio.SERVICE_STOP";
   private static final String CAMERA_ON_FM_OFF = "com.android.camera.cameracommand";
   private static final String FM_TOGGLEPAUSE_ACTION = "com.codeaurora.fmradio.command.togglepause";
   private static final String FM_PREVIOUS_ACTION = "com.codeaurora.fmradio.command.previous";
   private static final String FM_NEXT_ACTION = "com.codeaurora.fmradio.command.next";
   private static final String FM_CLOSE_ACTION = "com.codeaurora.fmradio.command.close";
   public static final String ACTION_FM = "codeaurora.intent.action.FM";
    public static final String ACTION_FM_RECORDING = "codeaurora.intent.action.FM_Recording";
   public static final String ACTION_FM_RECORDING_STATUS = "codeaurora.intent.action.FM.Recording.Status";
   private BroadcastReceiver mFmRecordingStatus  = null;
   static final int RECORD_START = 1;
   static final int RECORD_STOP = 0;
   private Thread mRecordServiceCheckThread = null;
   private static SharedPreferences msharedPreferences;  
   //renli add 20160525 
   public static final String FMCMD = "com.codeaurora.fmradio.fmcommand";
    //wangqi add for click HeadsethookDownLongPressed to close FM @20170314.
    public static final String FMCLOSE = "com.codeaurora.fmradio.close";

   public static final String CMDPREVIOUS = "previous";
   public static final String CMDNEXT = "next";
   private File mStoragePath = null;//motify by renli for bug397282  20160531
    //wangqi add for click HeadsethookDownLongPressed to close FM @20170314.
    private final BroadcastReceiver mcloseReceiver = new CloseFMBroadcastReceiver();
    public FMRadioService() {
   }
    //wangqi add for click HeadsethookDownLongPressed to close FM @20170314.
    private class CloseFMBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (isFmOn()) {
                fmOff();
            }

        }
    }

   @Override
   public void onCreate() {
      super.onCreate();

      mPrefs = new FmSharedPreferences(this);
      mCallbacks = null;
      TelephonyManager tmgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
      // caoyang_20140728 delete: Remove listener to call state. @{
      //tmgr.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE |
      //                                 PhoneStateListener.LISTEN_DATA_ACTIVITY);
      // @}
      PowerManager pm = (PowerManager)getSystemService(Context.POWER_SERVICE);
      mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, this.getClass().getName());
      mWakeLock.setReferenceCounted(false);
      misAnalogModeSupported  = SystemProperties.getBoolean("ro.fm.analogpath.supported",false);
      /* Register for Screen On/off broadcast notifications */
      mA2dpDeviceState = new A2dpDeviceStatus(getApplicationContext());
      registerScreenOnOffListener();
      registerHeadsetListener();
      registerExternalStorageListener();
      //if(!Build.CUSTOM_NAME.contains("Lenovo")) {
          registerAirplaneModeChangeListener();
      //}
      // Register broadcastReceiver for handling system language changed event
      registerSleepExpired();
      registerRecordTimeout();
      registerDelayedServiceStop();
      registerFMRecordingStatus();
      //registerLocaleChangeListener();
      // registering media button receiver seperately as we need to set
      // different priority for receiving media events
      registerFmMediaButtonReceiver();
	  registerAudioBecomeNoisy();
      //registerFmExternalStopReceiver();
      
      if ( false == SystemProperties.getBoolean("ro.fm.mulinst.recording.support",true)) {
           mSingleRecordingInstanceSupported = true;
      }

      // Register for pause commands from other apps to stop FM
      registerMusicServiceCommandReceiver();

      // If the service was idle, but got killed before it stopped itself, the
      // system will relaunch it. Make sure it gets stopped again in that case.

      setAlarmDelayedServiceStop();
      /* Query to check is a2dp supported in Hal */
       mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      String valueStr = mAudioManager.getParameters("isA2dpDeviceSupported");
      mA2dpDeviceSupportInHal = valueStr.contains("=true");
      Log.d(LOGTAG, " is A2DP device Supported In HAL"+mA2dpDeviceSupportInHal);
      //renli add 20160525
	  IntentFilter fmFilter = new IntentFilter();
      fmFilter.addAction(FMCMD);
      registerReceiver(fmReceiver, fmFilter);
	  //20160525
    //wangqi add for click HeadsethookDownLongPressed to close FM @20170314.
       IntentFilter closeFMFilter = new IntentFilter();
       closeFMFilter.addAction("com.codeaurora.fmradio.close");
       registerReceiver(mcloseReceiver, closeFMFilter);

   }

   @Override
   public void onDestroy() {
      Log.d(LOGTAG, "onDestroy");
      if (isFmOn())
      {
         Log.e(LOGTAG, "Service being destroyed while still playing.");
      }

      // make sure there aren't any other messages coming
      mDelayedStopHandler.removeCallbacksAndMessages(null);
      mVolumeToastHandler.removeCallbacksAndMessages(null);
      cancelAlarms();
      //release the audio focus listener
      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      audioManager.abandonAudioFocus(mAudioFocusListener);
      /* Remove the Screen On/off listener */
      if (mScreenOnOffReceiver != null) {
          unregisterReceiver(mScreenOnOffReceiver);
          mScreenOnOffReceiver = null;
      }
      /* Unregister the headset Broadcase receiver */
      if (mHeadsetReceiver != null) {
          unregisterReceiver(mHeadsetReceiver);
          mHeadsetReceiver = null;
      }
      if( mSdcardUnmountReceiver != null ) {
          unregisterReceiver(mSdcardUnmountReceiver);
          mSdcardUnmountReceiver = null;
      }
      if( mMusicCommandListener != null ) {
          unregisterReceiver(mMusicCommandListener);
          mMusicCommandListener = null;
      }
      if( mFmMediaButtonListener != null ) {
          unregisterReceiver(mFmMediaButtonListener);
          mFmMediaButtonListener = null;
      }
      if (mAudioBecomeNoisyListener != null) {
          unregisterReceiver(mAudioBecomeNoisyListener);
          mAudioBecomeNoisyListener = null;
      }
      if (mFMExternalStopReceiver != null){
          unregisterReceiver(mFMExternalStopReceiver);
          mFMExternalStopReceiver = null;
      }
      if(mAirplaneModeChangeListener != null){
          unregisterReceiver(mAirplaneModeChangeListener);
          mAirplaneModeChangeListener = null;
      }
      // Unregister the locale BroadcastReceiver
      if (mLocaleReceiver != null) {
          unregisterReceiver(mLocaleReceiver);
          mLocaleReceiver = null;
      }
      if(mSleepExpiredListener != null){
          unregisterReceiver(mSleepExpiredListener);
          mSleepExpiredListener = null;
      }
      if(mRecordTimeoutListener != null){
          unregisterReceiver(mRecordTimeoutListener);
          mRecordTimeoutListener = null;
      }
      if(mDelayedServiceStopListener != null){
          unregisterReceiver(mDelayedServiceStopListener);
          mDelayedServiceStopListener = null;
      }
      if (mFmRecordingStatus != null ) {
          unregisterReceiver(mFmRecordingStatus);
          mFmRecordingStatus = null;
      }
      /* Since the service is closing, disable the receiver */
      fmOff();

      TelephonyManager tmgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
      // caoyang_20140728 delete: Remove listener to call state. @{
      //tmgr.listen(mPhoneStateListener, 0);
      // @}

      Log.d(LOGTAG, "onDestroy: unbindFromService completed");
    //wangqi add for click HeadsethookDownLongPressed to close FM @20170314.
       unregisterReceiver(mcloseReceiver);
       unregisterReceiver(fmReceiver);//renli add 20160525
      mWakeLock.release();
      isExiting = false;
      super.onDestroy();
   }

   public void registerFMRecordingStatus() {
         if (mFmRecordingStatus == null) {
             mFmRecordingStatus = new BroadcastReceiver() {
                 @Override
                 public void onReceive(Context context, Intent intent) {
                     Log.d(LOGTAG, "received intent " +intent);
                     String action = intent.getAction();
                     if (action.equals(ACTION_FM_RECORDING_STATUS)) {
                         Log.d(LOGTAG, "ACTION_FM_RECORDING_STATUS Intent received");
                         int state = intent.getIntExtra("state", 0);
                         if (state == RECORD_START) {
                             Log.d(LOGTAG, "FM Recording started");
                             mFmRecordingOn = true;
                             mSampleStart = SystemClock.elapsedRealtime();
                             try {
                                  if ((mServiceInUse) && (mCallbacks != null) ) {
                                      Log.d(LOGTAG, "start recording thread");
                                      mCallbacks.onRecordingStarted();
                                  }
                                  startRecordServiceStatusCheck();
                             } catch (RemoteException e) {
                                  e.printStackTrace();
                             }
                         } else if (state == RECORD_STOP) {
                             Log.d(LOGTAG, "FM Recording stopped");
                             mFmRecordingOn = false;
                             try {
                                  if ((mServiceInUse) && (mCallbacks != null) ) {
                                     mCallbacks.onRecordingStopped();
                                  }
                             } catch (RemoteException e) {
                                  e.printStackTrace();
                             }
                             mSampleStart = 0;
                             stopRecordServiceStatusCheck();
                        }
                     }
                 }
             };
             IntentFilter iFilter = new IntentFilter();
             iFilter.addAction(ACTION_FM_RECORDING_STATUS);
             registerReceiver(mFmRecordingStatus , iFilter);
         }
   }

/**
      * Registers an intent to listen for ACTION_MEDIA_UNMOUNTED notifications.
      * The intent will call closeExternalStorageFiles() if the external media
      * is going to be ejected, so applications can clean up.
      */
     public void registerExternalStorageListener() {
         if (mSdcardUnmountReceiver == null) {
             mSdcardUnmountReceiver = new BroadcastReceiver() {
                 @Override
                 public void onReceive(Context context, Intent intent) {
                     String action = intent.getAction();
                     if ((action.equals(Intent.ACTION_MEDIA_UNMOUNTED))
                           || (action.equals(Intent.ACTION_MEDIA_EJECT))) {
                           // add by lihongya 20150121.
                           msharedPreferences = context.getSharedPreferences("FmRecordPath", Context.MODE_PRIVATE);
				int mPath = msharedPreferences.getInt("RecordPath", 0); 
                         Log.d(LOGTAG, "ACTION_MEDIA_UNMOUNTED Intent received");
                         if ((mFmRecordingOn == true)&&(mPath==1)) { //add by lihongya .recording in phone storage. don't stop recording .
                             try {
                                  stopRecording();
                             } catch (Exception e) {
                                  e.printStackTrace();
                             }
                         }
                     }
                 }
             };
             IntentFilter iFilter = new IntentFilter();
             iFilter.addAction(Intent.ACTION_MEDIA_UNMOUNTED);
             iFilter.addAction(Intent.ACTION_MEDIA_EJECT);
             iFilter.addDataScheme("file");
             registerReceiver(mSdcardUnmountReceiver, iFilter);
         }
     }

    /**
     * Register an intent to listen for airplane mode change.
     */
    public void registerAirplaneModeChangeListener(){
        if(mAirplaneModeChangeListener == null){
            mAirplaneModeChangeListener = new BroadcastReceiver(){

                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    String action = intent.getAction();
                    if(action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)){
                        boolean enabled = intent.getBooleanExtra("state", false);
                        Log.d(LOGTAG,"airplane enable state is:" + enabled);
                        if(enabled){
                            //exit FM Radio
                            fmOff();
                        }
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
            registerReceiver(mAirplaneModeChangeListener, iFilter);
        }
    }

     /**
     * Registers an intent to listen for ACTION_LOCALE_CHANGED
     * notifications. This intent is called to know if the system language changed
     */
    public void registerLocaleChangeListener() {
        if (mLocaleReceiver == null) {
            mLocaleReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (intent != null && intent.getAction().equals(Intent.ACTION_LOCALE_CHANGED)) {
                        mHandler.post(mLocalChangedHandler);
                    }
                }
            };
        }
        IntentFilter iFilter = new IntentFilter();
        iFilter.addAction(Intent.ACTION_LOCALE_CHANGED);
        registerReceiver(mLocaleReceiver, iFilter);
    }

    // Runnable posted to UI thread to update notification contents when system language changed
    final Runnable mLocalChangedHandler = new Runnable() {
        public void run() {
            startNotification();
        }
    };

     /**
     * Registers an intent to listen for ACTION_HEADSET_PLUG
     * notifications. This intent is called to know if the headset
     * was plugged in/out
     */
    public void registerHeadsetListener() {
        if (mHeadsetReceiver == null) {
            mHeadsetReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(Intent.ACTION_HEADSET_PLUG)) {
                       Log.d(LOGTAG, "ACTION_HEADSET_PLUG Intent received");
                       // Listen for ACTION_HEADSET_PLUG broadcasts.
                       Log.d(LOGTAG, "mReceiver: ACTION_HEADSET_PLUG");
                       Log.d(LOGTAG, "==> intent: " + intent);
                       Log.d(LOGTAG, "    state: " + intent.getIntExtra("state", 0));
                       Log.d(LOGTAG, "    name: " + intent.getStringExtra("name"));
                       mHeadsetPlugged = (intent.getIntExtra("state", 0) == 1);
                       //modify by puqirui @2015.1.27  
					   if (!Build.CUSTOM_NAME.contains("Micromax")) {
                          if (!mHeadsetPlugged && FMRadioService.this.mFMOn) {
                           // Notify FM radio was turned off cause by headset was plugged out.
                           isExiting = true;
                           Toast.makeText(context, R.string.msg_headset_plug_out, Toast.LENGTH_LONG).show();
                          } else {
                          	isExiting = false;
                          }
					   }
                       // if headset is plugged out it is required to disable
                       // in minimal duration to avoid race conditions with
                       // audio policy manager switch audio to speaker.
                       mHandler.removeCallbacks(mHeadsetPluginHandler);
					   
					   if (!Build.CUSTOM_NAME.contains("Micromax")) {
					      if(mHeadsetPlugged == false
								&& isCallActive()) {// fix by lihongya,20150120
						  FMRadioService.this.stopSelf();
					      }
					   }
                       
                       mHandler.post(mHeadsetPluginHandler);
                    } else if(mA2dpDeviceState.isA2dpStateChange(action) ) {
                        boolean  bA2dpConnected =
                        mA2dpDeviceState.isConnected(intent);
                        if (!bA2dpConnected) {
                            Log.d(LOGTAG, "A2DP device is dis-connected!");
                            mA2dpDisconnected = true;
                        }
                        if (isAnalogModeEnabled()) {
                            Log.d(LOGTAG, "FM Audio Path is Analog Mode: FM Over BT not allowed");
                            return ;
                        }
                       //when playback is overA2Dp and A2dp disconnected
                       //when playback is not overA2DP and A2DP Connected
                       // In above two cases we need to Stop and Start FM which
                       // will take care of audio routing
                       if( (isFmOn()) &&
                           (true == ((bA2dpConnected)^(mOverA2DP))) &&
                           (false == mStoppedOnFocusLoss) &&
                           (!isSpeakerEnabled())) {
                           stopFM();
                           startFM();
                       }
                    } else if (action.equals("HDMI_CONNECTED")) {
                        //FM should be off when HDMI is connected.
                        fmOff();
                        try
                        {
                            /* Notify the UI/Activity, only if the service is "bound"
                               by an activity and if Callbacks are registered
                             */
                            if((mServiceInUse) && (mCallbacks != null) )
                            {
                                mCallbacks.onDisabled();
                            }
                        } catch (RemoteException e)
                        {
                            e.printStackTrace();
                        }
                    } else if( action.equals(Intent.ACTION_SHUTDOWN)) {
                        mAppShutdown = true;
                        //caoyang_20130911 bug146003 Add:off FM when shutdown.{@
                        fmOff();
                        try {
                            /*
                             * Notify the UI/Activity, only if the service is
                             * "bound" by an activity and if Callbacks are
                             * registered
                             */
                            if ((mServiceInUse) && (mCallbacks != null)) {
                                mCallbacks.onDisabled();
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        //@}
                    } else if(action.equals(AudioManager.VOLUME_CHANGED_ACTION)) {
                        int streamType = intent.getIntExtra(AudioManager.EXTRA_VOLUME_STREAM_TYPE, -1);
                        if(AudioManager.STREAM_MUSIC == streamType) {
                            int volumeIndex = intent.getIntExtra(AudioManager.EXTRA_VOLUME_STREAM_VALUE, -1);
                            if(Build.CUSTOM_NAME.contains("Lenovo")) {
                                if(!isSpeakerEnabled() && isPlayback()) {
                                    if(volumeIndex >= 11) {
                                        Log.d(LOGTAG, "Volume changed(>=11). Send volume toast message.");
                                        mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                                        Message msg = mVolumeToastHandler.obtainMessage(VOLUME_TOAST);
                                        mVolumeToastHandler.sendMessageDelayed(msg, VOLUME_TOAST_DELAYED);
                                    } else {
                                        Log.d(LOGTAG, "Volume changed(<11). Remove volume toast message.");
                                        mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                                    }
                                }
                            }
                        }
                    }

                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_HEADSET_PLUG);
            iFilter.addAction(mA2dpDeviceState.getActionSinkStateChangedString());
            iFilter.addAction("HDMI_CONNECTED");
            iFilter.addAction(Intent.ACTION_SHUTDOWN);
            iFilter.addAction(AudioManager.VOLUME_CHANGED_ACTION);
            iFilter.addCategory(Intent.CATEGORY_DEFAULT);
            registerReceiver(mHeadsetReceiver, iFilter);
        }
    }

    public void registerFmMediaButtonReceiver() {
        if (mFmMediaButtonListener == null) {
            mFmMediaButtonListener = new BroadcastReceiver() {
                 public void onReceive(Context context, Intent intent) {
                     Log.d(LOGTAG, "FMMediaButtonIntentReceiver.FM_MEDIA_BUTTON");
                     Log.d(LOGTAG, "KeyEvent = " +intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT));
                     String action = intent.getAction();
                     if (action.equals(FMMediaButtonIntentReceiver.FM_MEDIA_BUTTON)) {
                         KeyEvent event = (KeyEvent)
                                     intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
                         int keycode = event.getKeyCode();
                         switch (keycode) {
                             case KeyEvent.KEYCODE_HEADSETHOOK :
                                 //caoyang_20130904 bug138065 Modify: {@
                                 // LongPress: Exit FM;
                                 // Short Press: Mute/unMute.
                                 if((event.getFlags() & KeyEvent.FLAG_LONG_PRESS) != 0) {
                                     if (isFmOn()){
                                         //FM should be off when Headset hook pressed.
                                         fmOff();
                                         if (isOrderedBroadcast()) {
                                            abortBroadcast();
                                         }
                                         try {
                                             /* Notify the UI/Activity, only if the service is "bound"
                                                by an activity and if Callbacks are registered
                                              */
                                             if ((mServiceInUse) && (mCallbacks != null) ) {
                                                mCallbacks.onDisabled();
                                             }
                                         } catch (RemoteException e) {
                                             e.printStackTrace();
                                         }
                                     }
                                 } else {
                                     if(mPlaybackInProgress) {
                                         stopFM();
										 stopRecording();//add by renli for bug406610 20160627
                                     } else {
                                         startFM();
                                     }
                                     //caoyang_20130912 Modify:fm_lenovo_ui {@
                                     if (Build.CUSTOM_NAME.contains("Lenovo")) {
                                         startNotification();
                                     }//@}
                                 }//@}
                                 break;
                             case KeyEvent.KEYCODE_MEDIA_PAUSE :
                                 if (isFmOn()){
                                     //FM should be off when Headset hook pressed.
                                     fmOff();
                                     if (isOrderedBroadcast()) {
                                        abortBroadcast();
                                     }
                                     try {
                                         /* Notify the UI/Activity, only if the service is "bound"
                                            by an activity and if Callbacks are registered
                                          */
                                         if ((mServiceInUse) && (mCallbacks != null) ) {
                                            mCallbacks.onDisabled();
                                         }
                                     } catch (RemoteException e) {
                                         e.printStackTrace();
                                     }
                                 }
                                 break;
                             case KeyEvent.KEYCODE_MEDIA_PLAY:
                                 if (isAntennaAvailable() && mServiceInUse ) {
                                     fmOn();
                                     if (isOrderedBroadcast()) {
                                         abortBroadcast();
                                     }
                                     try {
                                          /* Notify the UI/Activity, only if the service is "bound"
                                             by an activity and if Callbacks are registered
                                           */
                                          if (mCallbacks != null ) {
                                               mCallbacks.onEnabled();
                                          }
                                     } catch (RemoteException e) {
                                          e.printStackTrace();
                                     }
                                 }
                                 break;
                         } // end of switch
                     } // end of FMMediaButtonIntentReceiver.FM_MEDIA_BUTTON
                 } // end of onReceive
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(FMMediaButtonIntentReceiver.FM_MEDIA_BUTTON);
            registerReceiver(mFmMediaButtonListener, iFilter);
         }
     }

    public void registerAudioBecomeNoisy() {
		Log.d(LOGTAG, "registerAudioBecomeNoisy");
        if (mAudioBecomeNoisyListener == null) {
            mAudioBecomeNoisyListener = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String intentAction = intent.getAction();
                    Log.d(LOGTAG, "FMMediaButtonIntentReceiver.AUDIO_BECOMING_NOISY, intentAction=" + intentAction);
                    if (FMMediaButtonIntentReceiver.AUDIO_BECOMING_NOISY.equals(intentAction)) {
                        // caoyang_20140902 bug245993 pasue FM instead fmOff @{
                        //pauseFM();
                        // @}
                    }
                }
            };
            IntentFilter intentFilter = new IntentFilter(FMMediaButtonIntentReceiver.AUDIO_BECOMING_NOISY);
            registerReceiver(mAudioBecomeNoisyListener, intentFilter);
        }
    }

    public void registerMusicServiceCommandReceiver() {
        if (mMusicCommandListener == null) {
            mMusicCommandListener = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();

                    if (action.equals("com.android.music.musicservicecommand")) {
                        String cmd = intent.getStringExtra("command");
                        Log.d(LOGTAG, "Music Service command : "+cmd+ " received");
                        if (cmd != null && cmd.equals("pause")) {
                            if (mA2dpDisconnected) {
                                Log.d(LOGTAG, "not to pause,this is a2dp disconnected's pause");
                                mA2dpDisconnected = false;
                                return;
                            }
                            if(isFmOn()){
                                fmOff();
                                if (isOrderedBroadcast()) {
                                    abortBroadcast();
                                }
                                try {
                                    /* Notify the UI/Activity, only if the service is "bound"
                                       by an activity and if Callbacks are registered
                                    */
                                    if((mServiceInUse) && (mCallbacks != null) ){
                                        mCallbacks.onDisabled();
                                    }
                                } catch (RemoteException e) {
                                    e.printStackTrace();
                                }
                            } else if (mServiceInUse) {
                                // stop the service if it is in use
                                mResumeAfterCall = false;
                                stopSelf(mServiceStartId);
                            }
                        } else if(cmd != null && cmd.equals("playing")) {
                            Log.v(LOGTAG, "received force stop fm when music playing.");
                            stopFM();
                            FMRadioService.this.stopSelf();
                        }
                    }
                }
            };
            IntentFilter commandFilter = new IntentFilter();
            commandFilter.addAction("com.android.music.musicservicecommand");
            registerReceiver(mMusicCommandListener, commandFilter);
        }
    }
    final Runnable    mHeadsetPluginHandler = new Runnable() {
        public void run() {
            /* Update the UI based on the state change of the headset/antenna*/
            if(!isAntennaAvailable())
            {
                /* Disable FM and let the UI know */
                if (isFmOn()) {
                    fmOff();
                    try {
                        /*
                         * Notify the UI/Activity, only if the service is
                         * "bound" by an activity and if Callbacks are
                         * registered
                         */
                        if ((mServiceInUse) && (mCallbacks != null)) {
                            mCallbacks.onDisabled();
                        }
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                    // caoyang_20131102 bug159051 Add @{
                    FMRadio.resetSleepAtPhoneTime();
                    // @}
                }
            }
            else
            {
                //if fm is exiting we do not need to turn it on when headset plug in.
                if(isExiting){
                    return;
                }
                /* headset is plugged back in,
               So turn on FM if:
               - FM is not already ON.
               - If the FM UI/Activity is in the foreground
                 (the service is "bound" by an activity
                  and if Callbacks are registered)
                 */
                if ((!isFmOn()) && (mServiceInUse)
                        && (mCallbacks != null))
                {
                    if( true != fmOn() ) {
                        return;
                    }
                    try
                    {
                        mCallbacks.onEnabled();
                    } catch (RemoteException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    };

    public void registerSleepExpired() {
        if (mSleepExpiredListener == null) {
            mSleepExpiredListener = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    Log.d(LOGTAG, "registerSleepExpired");
                    mWakeLock.acquire(10 * 1000);
                    fmOff();
                }

            };
            IntentFilter intentFilter = new IntentFilter(SLEEP_EXPIRED_ACTION);
            registerReceiver(mSleepExpiredListener, intentFilter);
        }
    }

    public void registerRecordTimeout(){
        if(mRecordTimeoutListener == null){
            mRecordTimeoutListener = new BroadcastReceiver(){

                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    Log.d(LOGTAG,"registerRecordTimeout");
                    mWakeLock.acquire(5 * 1000);
                    stopRecording();
                }};
                IntentFilter intentFilter = new IntentFilter(RECORD_EXPIRED_ACTION);
                registerReceiver(mRecordTimeoutListener, intentFilter);
        }
    }

    public void registerDelayedServiceStop(){
        if(mDelayedServiceStopListener == null){
            mDelayedServiceStopListener = new BroadcastReceiver(){

                @Override
                public void onReceive(Context context, Intent intent) {
                    // TODO Auto-generated method stub
                    Log.d(LOGTAG,"registerDelayedServiceStop");
                    mWakeLock.acquire(5 * 1000);
                    if(isFmOn() || mServiceInUse){
                        return;
                    }
                    stopSelf(mServiceStartId);
                }};
                IntentFilter intentFilter = new IntentFilter(SERVICE_DELAYED_STOP_ACTION);
                registerReceiver(mDelayedServiceStopListener, intentFilter);
        }
    }

   /* public void registerFmExternalStopReceiver(){
        if (mFMExternalStopReceiver == null) {
            mFMExternalStopReceiver = new BroadcastReceiver() {
                 public void onReceive(Context context, Intent intent) {
                     String action = intent.getAction();
                     Log.d(LOGTAG, "Received external stop event " + action);
                     if (action.equals(CAMERA_ON_FM_OFF)){
                        try{
    						Log.d(LOGTAG, "mIntentReceiver mServiceInUse="+mServiceInUse+" mCallbacks="+mCallbacks);
    						if((mServiceInUse) && (mCallbacks != null) ){
    							mCallbacks.onDisabled();
    						}else{
    							Log.d(LOGTAG, "mIntentReceiver stopSelf!!!");
    							stopSelf(mServiceStartId);
    						}
    						// caoyang_20120609 No notification when FM closed after Activity has been closed
    						Log.d(LOGTAG, "---toast3--headset_unplug_err-");
                            
    						Toast toast = Toast.makeText(getApplicationContext(),getString(R.string.headset_unplug_err), Toast.LENGTH_SHORT);
    						toast.setGravity(Gravity.CENTER, 0, 0);
    						toast.show();
    						
    					} catch (RemoteException e){
    						e.printStackTrace();
    					}
                     }
                 } // end of onReceive
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(CAMERA_ON_FM_OFF);
            registerReceiver(mFMExternalStopReceiver, iFilter);
         }
    }//fix by zhoumingliang
    */ 
   @Override
   public IBinder onBind(Intent intent) {
      mDelayedStopHandler.removeCallbacksAndMessages(null);
      cancelAlarms();
      mServiceInUse = true;
      /* Application/UI is attached, so get out of lower power mode */
      setLowPowerMode(false);
      Log.d(LOGTAG, "onBind");
      return mBinder;
   }

   @Override
   public void onRebind(Intent intent) {
      mDelayedStopHandler.removeCallbacksAndMessages(null);
      cancelAlarmDelayedServiceStop();
      mServiceInUse = true;
      /* Application/UI is attached, so get out of lower power mode */
      setLowPowerMode(false);
      Log.d(LOGTAG, "onRebind");
   }

   @Override
   public void onStart(Intent intent, int startId) {
      Log.d(LOGTAG, "onStart");
      isExiting = false;
      mServiceStartId = startId;
      // make sure the service will shut down on its own if it was
      // just started but not bound to and nothing is playing
      mDelayedStopHandler.removeCallbacksAndMessages(null);
      cancelAlarmDelayedServiceStop();
      setAlarmDelayedServiceStop();
   }

   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
       if (intent != null) {
           String action = intent.getAction();
           Log.d(LOGTAG, "caoyangfm action = " + action);
           if (FM_TOGGLEPAUSE_ACTION.equals(action)) {
               //caoyang_20131009 bug153331 Modify @{
               if (isFmOn()){
                   if(mPlaybackInProgress) {
                       pauseFM();
                   } else {
                       playFM();
                   }
               }
               //@}
           } else if(FM_PREVIOUS_ACTION.equals(action)) {
               seek(false);
           }else if(FM_NEXT_ACTION.equals(action)) {
               seek(true);
           }else if(FM_CLOSE_ACTION.equals(action)) {
               if (isFmOn()){
                   //FM should be off when Headset hook pressed.
                   fmOff();
                   try {
                       /* Notify the UI/Activity, only if the service is "bound"
                          by an activity and if Callbacks are registered
                       */
                       if ((mServiceInUse) && (mCallbacks != null) ) {
                           mCallbacks.onDisabled();
                       }
                   } catch (RemoteException e) {
                       e.printStackTrace();
                   }
               }/* else {
                   //caoyang_20130924 bug149809 Add @{
                   stopSelf(mServiceStartId);
                   //@}
               }*/
               //caoyang_20140107 bug76051 Add @{
               stopSelf(mServiceStartId);
               //@}
               //caoyang_20130924 bug150743 Add @{
               return START_STICKY;
               //@}
           }
           //caoyang_20131125 bug166898 Modify @{
           if(action != null) {
               startNotification();
           }
           // @}
       }
       return START_STICKY;
   }

   @Override
   public boolean onUnbind(Intent intent) {
      mServiceInUse = false;
      Log.d(LOGTAG, "onUnbind");
      fmOff();
      /* Application/UI is not attached, so go into lower power mode */
      unregisterCallbacks();
      setLowPowerMode(true);
      // FM is playing or it will resume playback after call, don't stop the service
      if (isFmOn() || mResumeAfterCall)
      {
         // something is currently playing, or will be playing once
         // an in-progress call ends, so don't stop the service now.
         return true;
      }
      stopSelf(mServiceStartId);
      return true;
   }

   private String getProcessName() {
      int id = Process.myPid();
      String myProcessName = this.getPackageName();

      ActivityManager actvityManager =
              (ActivityManager)this.getSystemService(this.ACTIVITY_SERVICE);
      List<RunningAppProcessInfo> procInfos =
              actvityManager.getRunningAppProcesses();

      for(RunningAppProcessInfo procInfo : procInfos) {
         if (id == procInfo.pid) {
              myProcessName = procInfo.processName;
         }
      }
      procInfos.clear();
      return myProcessName;
   }

   private void sendRecordIntent(int action) {
       Intent intent = new Intent(ACTION_FM_RECORDING);
       intent.putExtra("state", action);
       if(action == RECORD_START) {
          int mRecordDuration = -1;
          if(FmSharedPreferences.getRecordDuration() !=
             FmSharedPreferences.RECORD_DUR_INDEX_3_VAL) {
             mRecordDuration = (FmSharedPreferences.getRecordDuration() * 60 * 1000);
          }
          intent.putExtra("record_duration", mRecordDuration);
          // caoyang_20140720 Add: put recording file path.
          intent.putExtra("record_path", FmSharedPreferences.getRecordPath());
          intent.putExtra("process_name", getProcessName());
          intent.putExtra("process_id", Process.myPid());
        }
       Log.d(LOGTAG, "Sending Recording intent for = " +action);
       getApplicationContext().sendBroadcast(intent);
   }

   private void sendRecordServiceIntent(int action) {
       Intent intent = new Intent(ACTION_FM);
       intent.putExtra("state", action);
       intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
       Log.d(LOGTAG, "Sending Recording intent for = " +action);
       getApplicationContext().sendBroadcast(intent);
   }

   private void startFM(){
       Log.d(LOGTAG, "In startFM");
       if(true == mAppShutdown) { // not to send intent to AudioManager in Shutdown
           return;
       }
       if (isCallActive()) { // when Call is active never let audio playback
           mResumeAfterCall = true;
           return;
       }

       if ( true == mPlaybackInProgress ) // no need to resend event
           return;
       AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
       int granted = audioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC,
              AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
       if(granted != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
          Log.d(LOGTAG, "audio focuss couldnot be granted");
          return;
       }

       Log.d(LOGTAG,"FM registering for registerMediaButtonEventReceiver");
       ComponentName fmRadio = new ComponentName(this.getPackageName(),
                                  FMMediaButtonIntentReceiver.class.getName());
       mAudioManager.registerMediaButtonEventReceiver(fmRadio);
       mStoppedOnFocusLoss = false;

       if (!isSpeakerEnabled() && !mA2dpDeviceSupportInHal &&  (true == mA2dpDeviceState.isDeviceAvailable()) &&
            (!isSpeakerEnabled()) && !isAnalogModeEnabled()
            && (true == startA2dpPlayback())) {
            mOverA2DP=true;
       } else {
           Log.d(LOGTAG, "FMRadio: sending the intent");
           // caoyang_20140905 bug247380: set Speaker before Available FM, avoid output sound from headset shortly.
           if (isSpeakerEnabled()) {
                   mSpeakerPhoneOn = true;
                   AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_SPEAKER);
           }
           //reason for resending the Speaker option is we are sending
           //ACTION_FM=1 to AudioManager, the previous state of Speaker we set
           //need not be retained by the Audio Manager.
           AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                                     AudioSystem.DEVICE_STATE_AVAILABLE, "","FmRadio");
       }
       sendRecordServiceIntent(RECORD_START);
       mPlaybackInProgress = true;
       try {
           if((mServiceInUse) && (mCallbacks != null) ) {
               mCallbacks.onPlayPause(mPlaybackInProgress);
           }
       } catch (RemoteException e)
       {
           e.printStackTrace();
       }

        // caoyang_20140722 Add: High volume toast. @{
        if(Build.CUSTOM_NAME.contains("Lenovo")) {
            if(!isSpeakerEnabled() && mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) >= 11) {
                Log.d(LOGTAG, "Playstate changed(play). Send volume toast message.");
                mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                Message msg = mVolumeToastHandler.obtainMessage(VOLUME_TOAST);
                mVolumeToastHandler.sendMessageDelayed(msg, VOLUME_TOAST_DELAYED);
            }
        }
        // @}
   }

   private void stopFM(){
       Log.d(LOGTAG, "In stopFM");
       if (mOverA2DP==true){
           mOverA2DP=false;
           stopA2dpPlayback();
       }else{
           Log.d(LOGTAG, "FMRadio: Requesting to stop FM");
           AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                                    AudioSystem.DEVICE_STATE_UNAVAILABLE, "","FmRadio");
       }
       mPlaybackInProgress = false;
       try {
           if((mServiceInUse) && (mCallbacks != null) ) {
               mCallbacks.onPlayPause(mPlaybackInProgress);
           }
       } catch (RemoteException e)
       {
           e.printStackTrace();
       }

        // caoyang_20140722 Add: High volume toast. @{
        if(Build.CUSTOM_NAME.contains("Lenovo")) {
            Log.d(LOGTAG, "Playstate changed(pause). Remove volume toast message.");
            mVolumeToastHandler.removeMessages(VOLUME_TOAST);
        }
        // @}
   }

   private void resetFM(){
       Log.d(LOGTAG, "resetFM");
       if (mOverA2DP==true){
           mOverA2DP=false;
           resetA2dpPlayback();
       }else{
          Log.d(LOGTAG, "FMRadio: Requesting to stop FM");
          AudioSystem.setDeviceConnectionState(AudioSystem.DEVICE_OUT_FM,
                                    AudioSystem.DEVICE_STATE_UNAVAILABLE, "","FmRadio");
           sendRecordServiceIntent(RECORD_STOP);
       }
       mPlaybackInProgress = false;
   }

   private boolean getRecordServiceStatus() {
       boolean status = false;
       ActivityManager actvityManager =
                (ActivityManager)this.getSystemService(this.ACTIVITY_SERVICE);
       List<RunningAppProcessInfo> procInfos =
                                    actvityManager.getRunningAppProcesses();
       for(RunningAppProcessInfo procInfo : procInfos) {
           if (procInfo.processName.equals("com.codeaurora.fmrecording")) {
               status = true;
               break;
           }
       }
       procInfos.clear();
       return status;
   }

   private Runnable recordStatusCheckThread = new Runnable() {
       @Override
       public void run() {
          while(!Thread.currentThread().isInterrupted()) {
               try {
                     if(!getRecordServiceStatus()) {
                        Log.d(LOGTAG, "FM Recording Service stopped");
                        mFmRecordingOn = false;
                        try {
                            if ((mServiceInUse) && (mCallbacks != null) ) {
                                Log.d(LOGTAG, "Callback for stop recording");
                                mCallbacks.onRecordingStopped();
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                        mSampleStart = 0;
                        break;
                     };
                     Thread.sleep(500);
                }catch(Exception e) {
                     Log.d(LOGTAG, "RecordService status check thread interrupted");
                     break;
                }
          }
       }
   };

   private void startRecordServiceStatusCheck() {
       if((mRecordServiceCheckThread == null) ||
          (mRecordServiceCheckThread.getState() == Thread.State.TERMINATED)) {
           mRecordServiceCheckThread = new Thread(null,
                                           recordStatusCheckThread,
                                           "getRecordServiceStatus");
       }
       if((mRecordServiceCheckThread != null) &&
          (mRecordServiceCheckThread.getState() == Thread.State.NEW)) {
           mRecordServiceCheckThread.start();
       }
   }

   private void stopRecordServiceStatusCheck() {
       if(mRecordServiceCheckThread != null) {
          mRecordServiceCheckThread.interrupt();
       }
   }
   		//add by renli for bug397282  20160531
       private File createTempFile(String prefix, String suffix, File directory)
            throws IOException {
        // Force a prefix null check first
        if (prefix.length() < 3) {
            throw new IllegalArgumentException("prefix must be at least 3 characters");
        }
        if (suffix == null) {
            suffix = ".tmp";
        }
        File tmpDirFile = directory;
        if (tmpDirFile == null) {
            String tmpDir = System.getProperty("java.io.tmpdir", ".");
            tmpDirFile = new File(tmpDir);
        }

        String nameFormat = getResources().getString(R.string.def_save_name_format);
        SimpleDateFormat df = new SimpleDateFormat(nameFormat);
        String currentTime = df.format(System.currentTimeMillis());

        File result;
        do {
            result = new File(tmpDirFile, prefix + currentTime + suffix);
        } while (!result.createNewFile());
        return result;
   }

   public boolean startRecording() {
      int mRecordDuration = -1;
       Log.d(LOGTAG, "In startRecording of Recorder");
       /*if (!getRecordServiceStatus()) {
           Log.d(LOGTAG, "Recording Service is not in running state");
           sendRecordServiceIntent(RECORD_START);
           try {
               Thread.sleep(200);
           } catch (Exception ex) {
               Log.d( LOGTAG, "RunningThread InterruptedException");
               return false;
           }
       }*/
       if ((true == mSingleRecordingInstanceSupported) &&
                               (true == mOverA2DP )) {
            Toast.makeText( this,
                      "playback on BT in progress,can't record now",
                       Toast.LENGTH_SHORT).show();
            return false;
       }
      // sendRecordIntent(RECORD_START);
stopRecording();

       if (!Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            Log.e(LOGTAG, "startRecording, no external storage available");
            return false;
       }

        if (!updateAndShowStorageHint())
            return false;
        long maxFileSize = mStorageSpace - LOW_STORAGE_THRESHOLD;
        if(FmSharedPreferences.getRecordDuration() !=
            FmSharedPreferences.RECORD_DUR_INDEX_3_VAL) {
            mRecordDuration = (FmSharedPreferences.getRecordDuration() * 60 * 1000);
         }

        mRecorder = new MediaRecorder();
        try {
              mRecorder.setMaxFileSize(maxFileSize);
              if (mRecordDuration >= 0)
                  mRecorder.setMaxDuration(mRecordDuration);
        } catch (RuntimeException exception) {

        }

        mStoragePath = Environment.getExternalStorageDirectory();
        Log.d(LOGTAG, "mStoragePath " + mStoragePath);
        if (null == mStoragePath) {
            Log.e(LOGTAG, "External Storage Directory is null");
            return false;
        }

        mSampleFile = null;
        File sampleDir = null;
        if (!"".equals(getResources().getString(R.string.def_fmRecord_savePath))) {
            String fmRecordSavePath = getResources().getString(R.string.def_fmRecord_savePath);
            sampleDir = new File(Environment.getExternalStorageDirectory().toString()
                    + fmRecordSavePath);
        } else {
            sampleDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                    + "/FMRecording");
        }

        if(!(sampleDir.mkdirs() || sampleDir.isDirectory()))
            return false;
        try {
            if (getResources().getBoolean(R.bool.def_save_name_format_enabled)) {
                String suffix = getResources().getString(R.string.def_save_name_suffix);
                suffix = "".equals(suffix) ? ".mp3" : suffix;
                String prefix = getResources().getString(R.string.def_save_name_prefix) + '-';
                mSampleFile = createTempFile(prefix, suffix, sampleDir);
            } else {
                mSampleFile = File.createTempFile("FMRecording", ".mp3", sampleDir);
            }
        } catch (IOException e) {
            Log.e(LOGTAG, "Not able to access SD Card");
            Toast.makeText(this, "Not able to access SD Card", Toast.LENGTH_SHORT).show();
            return false;
        }

        try {
             Log.d(LOGTAG, "AudioSource.RADIO_TUNER" +MediaRecorder.AudioSource.RADIO_TUNER);
             mRecorder.setAudioSource(MediaRecorder.AudioSource.RADIO_TUNER);
             mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
             mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
             final int samplingRate = 44100;
             mRecorder.setAudioSamplingRate(samplingRate);
             final int bitRate = 128000;
             mRecorder.setAudioEncodingBitRate(bitRate);
             final int audiochannels = 2;
             mRecorder.setAudioChannels(audiochannels);
        } catch (RuntimeException exception) {
             mRecorder.reset();
             mRecorder.release();
             mRecorder = null;
             return false;
        }
        mRecorder.setOutputFile(mSampleFile.getAbsolutePath());
        try {
             mRecorder.prepare();
             Log.d(LOGTAG, "start");
             mRecorder.start();
        } catch (IOException e) {
             mRecorder.reset();
             mRecorder.release();
             mRecorder = null;
             return false;
        } catch (RuntimeException e) {
             mRecorder.reset();
             mRecorder.release();
             mRecorder = null;
             return false;
        }
        mFmRecordingOn = true;
        Log.d(LOGTAG, "mSampleFile.getAbsolutePath() " +mSampleFile.getAbsolutePath());
        mRecorder.setOnInfoListener(new MediaRecorder.OnInfoListener() {
             public void onInfo(MediaRecorder mr, int what, int extra) {
                 if ((what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED) ||
                     (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED)) {
                     if (mFmRecordingOn) {
                         Log.d(LOGTAG, "Maximum file size/duration reached, stop the recording");
                         stopRecording();
                     }
                     if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED)
                         // Show the toast.
                         Toast.makeText(FMRadioService.this, R.string.FMRecording_reach_size_limit,
                                        Toast.LENGTH_LONG).show();
                 }
             }
             // from MediaRecorder.OnErrorListener
             public void onError(MediaRecorder mr, int what, int extra) {
                 Log.e(LOGTAG, "MediaRecorder error. what=" + what + ". extra=" + extra);
                 if (what == MediaRecorder.MEDIA_RECORDER_ERROR_UNKNOWN) {
                     // We may have run out of space on the sdcard.
                     if (mFmRecordingOn) {
                         stopRecording();
                     }
                     updateAndShowStorageHint();
                 }
             }
        });

        mSampleStart = SystemClock.elapsedRealtime();
        Log.d(LOGTAG, "mSampleStart: " +mSampleStart);
        return true;
   }
   //20160531
   public boolean startA2dpPlayback() {
        Log.d(LOGTAG, "In startA2dpPlayback");
    if( (true == mSingleRecordingInstanceSupported) &&
        (true == mFmRecordingOn )) {
                Toast.makeText(this,
                               "Recording already in progress,can't play on BT",
                               Toast.LENGTH_SHORT).show();
                return false;
       }
        if(mOverA2DP)
           stopA2dpPlayback();
        mA2dp = new MediaRecorder();
        if (mA2dp == null) {
           Toast.makeText(this,"A2dpPlayback failed to create an instance",
                            Toast.LENGTH_SHORT).show();
           return false;
        }
        try {
            //mA2dp.setAudioSource(MediaRecorder.AudioSource.FM_RX_A2DP);
            mA2dp.setOutputFormat(MediaRecorder.OutputFormat.RAW_AMR);
            mA2dp.setAudioEncoder(MediaRecorder.OutputFormat.DEFAULT);
            File sampleDir = new File(getFilesDir().getAbsolutePath());
            try {
                mA2DPSampleFile = File
                    .createTempFile("FMRecording", ".mp3", sampleDir);
            } catch (IOException e) {
                Log.e(LOGTAG, "Not able to access Phone's internal memory");
                Toast.makeText(this, "Not able to access Phone's internal memory",
                                Toast.LENGTH_SHORT).show();
                return false;
            }
            mA2dp.setOutputFile(mA2DPSampleFile.getAbsolutePath());
            mA2dp.prepare();
            mA2dp.start();
        } catch (Exception exception) {
            mA2dp.reset();
            mA2dp.release();
            mA2dp = null;
            return false;
        }
        return true;
 }

   public void stopA2dpPlayback() {
       if (mA2dp == null)
           return;
       if(mA2DPSampleFile != null)
       {
          try {
              mA2DPSampleFile.delete();
          } catch (Exception e) {
              Log.e(LOGTAG, "Not able to delete file");
          }
       }
       try {
           mA2dp.stop();
           mA2dp.reset();
           mA2dp.release();
           mA2dp = null;
       } catch (Exception exception ) {
           Log.e( LOGTAG, "Stop failed with exception"+ exception);
       }
       return;
   }

   private void resetA2dpPlayback() {
       if (mA2dp == null)
           return;
       if(mA2DPSampleFile != null)
       {
          try {
              mA2DPSampleFile.delete();
          } catch (Exception e) {
              Log.e(LOGTAG, "Not able to delete file");
          }
       }
       try {
           // Send Intent for IOBUSY VOTE, because MediaRecorder.stop
           // gets Activity context which might not be always available
           // and would thus fail to send the intent.
           Intent ioBusyUnVoteIntent = new Intent(IOBUSY_UNVOTE);
           // Remove vote for io_is_busy to be turned off.
           ioBusyUnVoteIntent.putExtra("com.android.server.CpuGovernorService.voteType", 0);
           sendBroadcast(ioBusyUnVoteIntent);

           mA2dp.stop();

           mA2dp.reset();
           mA2dp.release();
           mA2dp = null;
       } catch (Exception exception ) {
           Log.e( LOGTAG, "Stop failed with exception"+ exception);
       }
       return;
   }

   private void resetRecording() {

       Log.v(LOGTAG, "resetRecording()");

       mFmRecordingOn = false;
       if (mRecorder == null)
           return;

       // Send Intent for IOBUSY VOTE, because MediaRecorder.stop
       // gets Activity context which might not be always available
       // and would thus fail to send the intent.
       Intent ioBusyUnVoteIntent = new Intent(IOBUSY_UNVOTE);
       // Remove vote for io_is_busy to be turned off.
       ioBusyUnVoteIntent.putExtra("com.android.server.CpuGovernorService.voteType", 0);
       sendBroadcast(ioBusyUnVoteIntent);

       mRecorder.stop();

       mRecorder.reset();
       mRecorder.release();
       mRecorder = null;
       int sampleLength = (int)((System.currentTimeMillis() - mSampleStart)/1000 );
       if (sampleLength == 0)
           return;
       String state = Environment.getExternalStorageState();
       Log.d(LOGTAG, "storage state is " + state);

       if (Environment.MEDIA_MOUNTED.equals(state)) {
           this.addToMediaDB(mSampleFile);
       }
       else{
           Log.e(LOGTAG, "SD card must have removed during recording. ");
           Toast.makeText(this, R.string.Recording_aborted, Toast.LENGTH_SHORT).show();
       }
       return;
   }
	//motify by renli for bug397282  20160531
  public void stopRecording() {
       Log.d(LOGTAG, "Enter stopRecord");
       mFmRecordingOn = false;
       if (mRecorder == null)
           return;
       try {
             //mRecorder.stop(); motify by renli for bug402273 20160606
             mRecorder.reset();
             mRecorder.release();
             mRecorder = null;
       } catch(Exception e) {
             e.printStackTrace();
       }
       //start fix bug of LXF_P3590_B01-655 @20170406 add by chenhaoran
       mSampleDuration = SystemClock.elapsedRealtime() - mSampleStart;
       if (mSampleDuration == 0)
           return;
       //end
       String state = Environment.getExternalStorageState(mStoragePath);
       Log.d(LOGTAG, "storage state is " + state);

       if (Environment.MEDIA_MOUNTED.equals(state)) {
          try {
               this.addToMediaDB(mSampleFile);
               Toast.makeText(this,getString(R.string.save_record_file,
                              mSampleFile.getAbsolutePath( )),
                              Toast.LENGTH_LONG).show();
          } catch(Exception e) {
               e.printStackTrace();
          }
       } else {
           Log.e(LOGTAG, "SD card must have removed during recording. ");
           Toast.makeText(this, "Recording aborted", Toast.LENGTH_SHORT).show();
       }
       try {
           if((mServiceInUse) && (mCallbacks != null) ) {
               mCallbacks.onRecordingStopped();
           }
       } catch (RemoteException e) {
           e.printStackTrace();
       }
       return;
   }
   //20160531

   /*
    * Adds file and returns content uri.
    */
   private Uri addToMediaDB(File file) {
       Log.d(LOGTAG, "In addToMediaDB");
       Resources res = getResources();
       ContentValues cv = new ContentValues();
       long current = System.currentTimeMillis();
       long modDate = file.lastModified();
       Date date = new Date(current);
       SimpleDateFormat formatter = new SimpleDateFormat(
               res.getString(R.string.audio_db_title_format));
       String title = formatter.format(date);

       // Lets label the recorded audio file as NON-MUSIC so that the file
       // won't be displayed automatically, except for in the playlist.
       cv.put(MediaStore.Audio.Media.IS_MUSIC, "1");

       cv.put(MediaStore.Audio.Media.TITLE, title);
       cv.put(MediaStore.Audio.Media.DATA, file.getAbsolutePath());
       cv.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
       cv.put(MediaStore.Audio.Media.DATE_MODIFIED, (int) (modDate / 1000));
       cv.put(MediaStore.Audio.Media.DURATION, mSampleDuration);
       cv.put(MediaStore.Audio.Media.MIME_TYPE, "audio/aac");
       cv.put(MediaStore.Audio.Media.ARTIST,
               res.getString(R.string.audio_db_artist_name));
       cv.put(MediaStore.Audio.Media.ALBUM,
               res.getString(R.string.audio_db_album_name));
       Log.d(LOGTAG, "Inserting audio record: " + cv.toString());
       ContentResolver resolver = getContentResolver();
       Uri base = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
       Log.d(LOGTAG, "ContentURI: " + base);
       Uri result = resolver.insert(base, cv);
       if (result == null) {
	    //matao 20140108 W_And00180469
           //Toast.makeText(this, "Unable to save recorded audio", Toast.LENGTH_SHORT).show();
           return null;
       }
       if (getPlaylistId(res) == -1) {
           createPlaylist(res, resolver);
       }
       int audioId = Integer.valueOf(result.getLastPathSegment());
       addToPlaylist(resolver, audioId, getPlaylistId(res));

       // Notify those applications such as Music listening to the
       // scanner events that a recorded audio file just created.
       sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, result));
       return result;
   }

   private int getPlaylistId(Resources res) {
       Uri uri = MediaStore.Audio.Playlists.getContentUri("external");
       final String[] ids = new String[] { MediaStore.Audio.Playlists._ID };
       final String where = MediaStore.Audio.Playlists.NAME + "=?";
       final String[] args = new String[] { res.getString(R.string.audio_db_playlist_name) };
       Cursor cursor = query(uri, ids, where, args, null);
       if (cursor == null) {
           Log.v(LOGTAG, "query returns null");
       }
       int id = -1;
       if (cursor != null) {
           cursor.moveToFirst();
           if (!cursor.isAfterLast()) {
               id = cursor.getInt(0);
           }
           cursor.close();
       }
       return id;
   }

   private Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
       try {
           ContentResolver resolver = getContentResolver();
           if (resolver == null) {
               return null;
           }
           return resolver.query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (UnsupportedOperationException ex) {
           return null;
       }
   }

   private Uri createPlaylist(Resources res, ContentResolver resolver) {
       ContentValues cv = new ContentValues();
       cv.put(MediaStore.Audio.Playlists.NAME, res.getString(R.string.audio_db_playlist_name));
       Uri uri = resolver.insert(MediaStore.Audio.Playlists.getContentUri("external"), cv);
       if (uri == null) {
	    //matao 20140108 W_And00180469
           //Toast.makeText(this, "Unable to save recorded audio", Toast.LENGTH_SHORT).show();
       }
       return uri;
   }

   private void addToPlaylist(ContentResolver resolver, int audioId, long playlistId) {
       String[] cols = new String[] {
               "count(*)"
       };
       Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistId);
       Cursor cur = resolver.query(uri, cols, null, null, null);
       final int base;
       if (cur != null) {
            cur.moveToFirst();
            base = cur.getInt(0);
            cur.close();
       }
       else {
            base = 0;
       }
       ContentValues values = new ContentValues();
       values.put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, Integer.valueOf(base + audioId));
       values.put(MediaStore.Audio.Playlists.Members.AUDIO_ID, audioId);
       resolver.insert(uri, values);
   }
   private void fmActionOnCallState( int state ) {
   //if Call Status is non IDLE we need to Mute FM as well stop recording if
   //any. Similarly once call is ended FM should be unmuted.
       AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
       mCallStatus = state;

       if((TelephonyManager.CALL_STATE_OFFHOOK == state)||
          (TelephonyManager.CALL_STATE_RINGING == state)) {
           if (state == TelephonyManager.CALL_STATE_RINGING) {
               int ringvolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
               if (ringvolume == 0) {
                   return;
               }
           }
           boolean bTempSpeaker = mSpeakerPhoneOn; //need to restore SpeakerPhone
           boolean bTempMute = mMuted;// need to restore Mute status
           int bTempCall = mCallStatus;//need to restore call status
           mResumeAfterCall = true;
           if(TelephonyManager.CALL_STATE_RINGING == state) {
               Log.d(LOGTAG, "fmActionOnCallState1 CALL_STATE_RINGING mPlaybackInProgress = " + mPlaybackInProgress);
               if(mPlaybackInProgress) {
                   resumePlayback = true;
               } else {
                   resumePlayback = false;
               }
           }
           if (isFmOn() && fmOff()) {
               if((mServiceInUse) && (mCallbacks != null)) {
                   try {
                        mCallbacks.onDisabled();
                   } catch (RemoteException e) {
                        e.printStackTrace();
                   }
               }
               //mResumeAfterCall = true;
               mSpeakerPhoneOn = bTempSpeaker;
               mCallStatus = bTempCall;
               mMuted = bTempMute;
           } else if (!mResumeAfterCall) {
               mResumeAfterCall = false;
               mSpeakerPhoneOn = bTempSpeaker;
               mCallStatus = bTempCall;
               mMuted = bTempMute;
           }
       }
       else if (state == TelephonyManager.CALL_STATE_IDLE) {
          Log.d(LOGTAG, "fmActionOnCallState : CALL_STATE_IDLE mResumeAfterCall = " + mResumeAfterCall);
          // start playing again
          if (mResumeAfterCall)
          {
             // resume playback only if FM Radio was playing
             // when the call was answered
              if (isAntennaAvailable() && (!isFmOn()) && mServiceInUse)
              {
                   Log.d(LOGTAG, "Resuming after call:");
                   if(true != fmOn()) {
                       return;
                   }
                   tune(mPrefs.getTunedFrequency());
                   mResumeAfterCall = false;
                   // modify by qiuxiaowei 20140221
                   // fix bug 190204 (resume playback when call is idle)
                   // caoyang: delete this code , the playstate will be same with the state before the call.
                   //resumePlayback = true;
                   // end
                   if(mCallbacks != null) {
                      try {
                           mCallbacks.onEnabled();
                      } catch (RemoteException e) {
                           e.printStackTrace();
                      }
                   }
                } else if((isAntennaAvailable())
                         ) {
                    if (true != fmOn()) {
                        Log.d(LOGTAG, "fmOn failed.......");
                    } else {
                        tune(mPrefs.getTunedFrequency());
                    }
                    mResumeAfterCall = false;
                }
          }
       }//idle
   }

    /* Handle Phone Call + FM Concurrency */
/*   private PhoneStateListener getPhoneStateListener(int sub) {
       PhoneStateListener phoneStateListener = new PhoneStateListener(sub) {
           public void onCallStateChanged(int state, String incomingNumber) {
               Log.d(LOGTAG,"mSubscription:" + mSubscription);
               Log.d(LOGTAG, "onCallStateChanged mSubscription: State - " + state);
               Log.d(LOGTAG, "onCallStateChanged: incomingNumber - "
                       + incomingNumber);
               fmActionOnCallState(state);
           }
           @Override
           public void onDataActivity (int direction) {
               Log.d(LOGTAG, "onDataActivity - " + direction );
               if (direction == TelephonyManager.DATA_ACTIVITY_NONE ||
                   direction == TelephonyManager.DATA_ACTIVITY_DORMANT) {
                      if (mReceiver != null) {
                            Message msg = mDelayedStopHandler.obtainMessage(RESET_NOTCH_FILTER);
                            mDelayedStopHandler.sendMessageDelayed(msg, 10000);
                      }
              } else {
                    if (mReceiver != null) {
                        if( true == mNotchFilterSet )
                        {
                            mDelayedStopHandler.removeMessages(RESET_NOTCH_FILTER);
                        }
                        else
                        {
                            mReceiver.setNotchFilter(true);
                            mNotchFilterSet = true;
                        }
                    }
              }
           }
      };

 return phoneStateListener;
}*/

    /* Handle Phone Call + FM Concurrency */
   private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
      @Override
      public void onCallStateChanged(int state, String incomingNumber) {
          Log.d(LOGTAG, "onCallStateChanged: State - " + state );
          Log.d(LOGTAG, "onCallStateChanged: incomingNumber - " + incomingNumber );
          fmActionOnCallState(state );
      }

      @Override
      public void onDataActivity (int direction) {
          Log.d(LOGTAG, "onDataActivity - " + direction );
          if (direction == TelephonyManager.DATA_ACTIVITY_NONE ||
              direction == TelephonyManager.DATA_ACTIVITY_DORMANT) {
                 if (mReceiver != null) {
                       Message msg = mDelayedStopHandler.obtainMessage(RESET_NOTCH_FILTER);
                       mDelayedStopHandler.sendMessageDelayed(msg, 10000);
                 }
         } else {
               if (mReceiver != null) {
                   if( true == mNotchFilterSet )
                   {
                       mDelayedStopHandler.removeMessages(RESET_NOTCH_FILTER);
                   }
                   else
                   {
                       mReceiver.setNotchFilter(true);
                       mNotchFilterSet = true;
                   }
               }
         }
      }
 };
   private Handler mDelayedStopHandler = new Handler() {
      @Override
      public void handleMessage(Message msg) {
          switch (msg.what) {
          case FM_STOP:
              // Check again to make sure nothing is playing right now
              if (isFmOn() || mServiceInUse)
              {
                   return;
              }
              Log.d(LOGTAG, "mDelayedStopHandler: stopSelf");
              stopSelf(mServiceStartId);
              break;
          case RESET_NOTCH_FILTER:
              if (mReceiver != null) {
                  mReceiver.setNotchFilter(false);
                  mNotchFilterSet = false;
              }
              break;
          case STOPSERVICE_ONSLEEP:
              fmOff();
              break;
          case STOPRECORD_ONTIMEOUT:
              stopRecording();
              break;
          case FOCUSCHANGE:
              if( false == isFmOn() ) {
                  Log.v(LOGTAG, "FM is not running, not handling change");
                  return;
              }
              switch (msg.arg1) {
                  case AudioManager.AUDIOFOCUS_LOSS:
                      Log.v(LOGTAG, "AudioFocus: received AUDIOFOCUS_LOSS");
                      //intentional fall through.
                      //mResumeAfterCall = false;
                      stopFM();
                      FMRadioService.this.stopSelf();
                      break;
                  case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                      Log.v(LOGTAG, "AudioFocus: received AUDIOFOCUS_LOSS_TRANSIENT");
                      //mResumeAfterCall = false;
                      if(true == isFmRecordingOn()) {
                          stopRecording();
                      }
                      // caoyang_20140728 Modify: Remove listener to call state. @{
                      //if(mPlaybackInProgress || mResumeAfterCall) {
                      Log.d(LOGTAG, "mPlaybackInProgress = " + mPlaybackInProgress);
                      if(mPlaybackInProgress) {
                      // @}
                          stopFM();
                          resumePlayback = true;

                          // caoyang_20140722 Add: High volume toast. @{
                          if(Build.CUSTOM_NAME.contains("Lenovo")) {
                              Log.d(LOGTAG, "Audio Focust Loss transient. Remove volume toast message.");
                              mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                          }
                          // @}
                      } else {
                          resumePlayback = false;
                      }
                      // caoyang_20140905 bug247380: set Speaker after stop FM, avoid output sound from headset shortly.
                      if (mSpeakerPhoneOn) {
                          //mSpeakerPhoneOn = false;
                          resumeSpeaker = true;
                          if (isAnalogModeSupported()) {
                              setAudioPath(true);
                          }
                          AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_NONE);
                      }
                      startNotification();
                      mStoppedOnFocusLoss = true;
                      break;
                  case AudioManager.AUDIOFOCUS_GAIN:
                      Log.v(LOGTAG, "AudioFocus: received AUDIOFOCUS_GAIN");
                      Log.v(LOGTAG, "AudioFocus: received AUDIOFOCUS_GAIN  resumePlayback = " + resumePlayback);
                      Log.v(LOGTAG, "AudioFocus: received AUDIOFOCUS_GAIN  mPlaybackInProgress = " + mPlaybackInProgress);
                      if(resumePlayback && !mPlaybackInProgress){
                          startFM();
                      }
                      resumePlayback = true;
                      startNotification();
                      mStoppedOnFocusLoss = false;
                      break;
                  case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                  default:
                      Log.e(LOGTAG, "Unknown audio focus change code"+msg.arg1);
              }
              break;
          }
      }
   };


     /**
     * Registers an intent to listen for
     * ACTION_SCREEN_ON/ACTION_SCREEN_OFF notifications. This intent
     * is called to know iwhen the screen is turned on/off.
     */
    public void registerScreenOnOffListener() {
        if (mScreenOnOffReceiver == null) {
            mScreenOnOffReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(Intent.ACTION_SCREEN_ON)) {
                       Log.d(LOGTAG, "ACTION_SCREEN_ON Intent received");
                       //Screen turned on, set FM module into normal power mode
                       mHandler.post(mScreenOnHandler);
                    }
                    else if (action.equals(Intent.ACTION_SCREEN_OFF)) {
                       Log.d(LOGTAG, "ACTION_SCREEN_OFF Intent received");
                       //Screen turned on, set FM module into low power mode
                       mHandler.post(mScreenOffHandler);
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_SCREEN_ON);
            iFilter.addAction(Intent.ACTION_SCREEN_OFF);
            registerReceiver(mScreenOnOffReceiver, iFilter);
        }
    }

    /* Handle all the Screen On actions:
       Set FM Power mode to Normal
     */
    final Runnable    mScreenOnHandler = new Runnable() {
       public void run() {
          setLowPowerMode(false);
       }
    };
    /* Handle all the Screen Off actions:
       Set FM Power mode to Low Power
       This will reduce all the interrupts coming up from the SoC, saving power
     */
    final Runnable    mScreenOffHandler = new Runnable() {
       public void run() {
          setLowPowerMode(true);
       }
    };

    // caoyang_20140722 Add: High volume toast. @{
    private Handler mVolumeToastHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case VOLUME_TOAST:
                    mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                    Toast warningToast = Toast.makeText(FMRadioService.this,
                            com.android.internal.R.string.safe_media_volume_warning, Toast.LENGTH_SHORT);
                    warningToast.show();
                    msg = mVolumeToastHandler.obtainMessage(VOLUME_TOAST);
                    mVolumeToastHandler.sendMessageDelayed(msg, VOLUME_TOAST_DELAYED);
                    break;
                default :
                    break;
            }
        }
    };
    // @}

   /* Show the FM Notification */
   //caoyang_20130912 Modify:fm_lenovo_ui
   public void startNotification() {
       if (Build.CUSTOM_NAME.contains("Lenovo")) {
           RemoteViews views = new RemoteViews(getPackageName(), R.layout.newstatusbar);
           String trackinfo = getTunedFrequencyString();
           views.setTextViewText(R.id.txt_trackinfo, trackinfo);
           Intent intent;
           PendingIntent pIntent;

           intent = new Intent();
           intent.setClassName(getPackageName(), FMRadio.class.getName());
           pIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
           views.setOnClickPendingIntent(R.id.iv_cover, pIntent);

           intent = new Intent(FM_PREVIOUS_ACTION);
           intent.setClass(this, FMRadioService.class);
           pIntent = PendingIntent.getService(this, 0, intent, 0);
           views.setOnClickPendingIntent(R.id.btn_prev, pIntent);

           intent = new Intent(FM_TOGGLEPAUSE_ACTION);
           intent.setClass(this, FMRadioService.class);
           pIntent = PendingIntent.getService(this, 0, intent, 0);
           views.setOnClickPendingIntent(R.id.btn_pause, pIntent);
           //caoyang_20131009 bug153331 Modify @{
           if (!isFmOn()){
               views.setImageViewResource(R.id.btn_pause, R.drawable.stat_notif_play);
           } else {
               if(mPlaybackInProgress) {
                   views.setImageViewResource(R.id.btn_pause, R.drawable.stat_notif_pause);
               } else {
                   views.setImageViewResource(R.id.btn_pause, R.drawable.stat_notif_play);
               }
           }
           //@}

           intent = new Intent(FM_NEXT_ACTION);
           intent.setClass(this, FMRadioService.class);
           pIntent = PendingIntent.getService(this, 0, intent, 0);
           views.setOnClickPendingIntent(R.id.btn_next, pIntent);

           intent = new Intent(FM_CLOSE_ACTION);
           intent.setClass(this, FMRadioService.class);
           pIntent = PendingIntent.getService(this, 0, intent, 0);
           views.setOnClickPendingIntent(R.id.btn_close, pIntent);

           intent = new Intent("my.nullaction");
           pIntent = PendingIntent.getService(this, 0, intent, 0);
           views.setOnClickPendingIntent(R.id.rl_newstatus, pIntent);

           Notification status = new Notification();
           status.contentView = views;
           status.flags |= Notification.FLAG_ONGOING_EVENT;
           status.icon = R.drawable.stat_notify_fm;
           status.contentIntent = PendingIntent.getService(this, 0, intent, 0);
           startForeground(1, status);
       } else {
           Intent notificationIntent = new Intent();
           notificationIntent.setClassName(getPackageName(), FMRadio.class.getName());
           PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0, notificationIntent, 0);
           Notification notification = new Notification(R.drawable.stat_notify_fm, null, System.currentTimeMillis());
           notification.flags |= Notification.FLAG_ONGOING_EVENT | Notification.FLAG_NO_CLEAR;
           String text = getTunedFrequencyString();
           notification.setLatestEventInfo(getApplicationContext(), getResources().getString(R.string.app_name), text,
                   pendingIntent);
           startForeground(1, notification);
       }
   }
    /* caoyang_20130824 bug140201 Use uniform notification style.
    {
      RemoteViews views = new RemoteViews(getPackageName(), R.layout.statusbar);
      views.setImageViewResource(R.id.icon, R.drawable.stat_notify_fm);
      if (isFmOn())
      {
         views.setTextViewText(R.id.frequency, getTunedFrequencyString());
      } else
      {
         views.setTextViewText(R.id.frequency, "");
      }

      Notification status = new Notification();
      status.contentView = views;
      status.flags |= Notification.FLAG_ONGOING_EVENT;
      status.icon = R.drawable.stat_notify_fm;
      status.contentIntent = PendingIntent.getActivity(this, 0,
                                                       new Intent("com.quicinc.fmradio.FMRADIO_ACTIVITY"), 0);
      startForeground(FMRADIOSERVICE_STATUS, status);
      //NotificationManager nm = (NotificationManager)
      //                         getSystemService(Context.NOTIFICATION_SERVICE);
      //nm.notify(FMRADIOSERVICE_STATUS, status);
      //setForeground(true);
   }*/

   private void stop() {
      Log.d(LOGTAG,"in stop");
      //will exit in fmoff , no need exit here.
//      try {
//           if (mFMOn && (mCallbacks != null))
//               mCallbacks. onFinishActivity();
//      } catch (RemoteException e) {
//           e.printStackTrace();
//      }
       if (!mServiceInUse) {
          Log.d(LOGTAG,"calling unregisterMediaButtonEventReceiver in stop");
          ComponentName fmRadio = new ComponentName(this.getPackageName(),
                                  FMMediaButtonIntentReceiver.class.getName());
          mAudioManager.unregisterMediaButtonEventReceiver(fmRadio);
      }
      gotoIdleState();
      mFMOn = false;
   }

   private void gotoIdleState() {
      mDelayedStopHandler.removeCallbacksAndMessages(null);
      cancelAlarms();
      setAlarmDelayedServiceStop();
      //NotificationManager nm =
      //(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
      //nm.cancel(FMRADIOSERVICE_STATUS);
      //setForeground(false);
      stopForeground(true);
   }

   /** Read's the internal Antenna available state from the FM
    *  Device.
    */
   public void readInternalAntennaAvailable()
   {
      mInternalAntennaAvailable  = false;
      if (mReceiver != null)
      {
         mInternalAntennaAvailable = mReceiver.getInternalAntenna();
         Log.d(LOGTAG, "getInternalAntenna: " + mInternalAntennaAvailable);
      }
   }
   /*
    * By making this a static class with a WeakReference to the Service, we
    * ensure that the Service can be GCd even when the system process still
    * has a remote reference to the stub.
    */
   static class ServiceStub extends IFMRadioService.Stub
   {
      WeakReference<FMRadioService> mService;

      ServiceStub(FMRadioService service)
      {
         mService = new WeakReference<FMRadioService>(service);
      }

      public boolean fmOn() throws RemoteException
      {
         return(mService.get().fmOn());
      }

      public boolean fmOff() throws RemoteException
      {
         return(mService.get().fmOff());
      }

      public boolean fmRadioReset() throws RemoteException
      {
         return true;
      }

      public boolean isFmOn()
      {
         return(mService.get().isFmOn());
      }

      public boolean isAnalogModeEnabled()
      {
         return(mService.get().isAnalogModeEnabled());
      }

      public boolean isFmRecordingOn()
      {
         return(mService.get().isFmRecordingOn());
      }

      public boolean isResumeSpeaker()
      {
         return(mService.get().isResumeSpeaker());
      }

      public boolean isSpeakerEnabled()
      {
         return(mService.get().isSpeakerEnabled());
      }

      public boolean fmReconfigure()
      {
         return(mService.get().fmReconfigure());
      }

      public void registerCallbacks(IFMRadioServiceCallbacks cb) throws RemoteException
      {
         mService.get().registerCallbacks(cb);
      }

      public void unregisterCallbacks() throws RemoteException
      {
         mService.get().unregisterCallbacks();
      }

      public boolean routeAudio(int device)
      {
         return(mService.get().routeAudio(device));
      }

      public boolean mute()
      {
         return(mService.get().mute());
      }

      public boolean unMute()
      {
         return(mService.get().unMute());
      }

      public boolean isMuted()
      {
         return(mService.get().isMuted());
      }

      public boolean startRecording()
      {
         return(mService.get().startRecording());
      }

      public void stopRecording()
      {
         mService.get().stopRecording();
      }

      public boolean tune(int frequency)
      {
         return(mService.get().tune(frequency));
      }

      public boolean seek(boolean up)
      {
         return(mService.get().seek(up));
      }

      public void enableSpeaker(boolean speakerOn)
      {
          mService.get().enableSpeaker(speakerOn);
      }

      public boolean scan(int pty)
      {
         return(mService.get().scan(pty));
      }

      public boolean seekPI(int piCode)
      {
         return(mService.get().seekPI(piCode));
      }
      public boolean searchStrongStationList(int numStations)
      {
         return(mService.get().searchStrongStationList(numStations));
      }

      public boolean cancelSearch()
      {
         return(mService.get().cancelSearch());
      }

      public String getProgramService()
      {
         return(mService.get().getProgramService());
      }
      public String getRadioText()
      {
         return(mService.get().getRadioText());
      }
      public int getProgramType()
      {
         return(mService.get().getProgramType());
      }
      public int getProgramID()
      {
         return(mService.get().getProgramID());
      }
      public int[] getSearchList()
      {
         return(mService.get().getSearchList());
      }

      public boolean setLowPowerMode(boolean enable)
      {
         return(mService.get().setLowPowerMode(enable));
      }

      public int getPowerMode()
      {
         return(mService.get().getPowerMode());
      }
      public boolean enableAutoAF(boolean bEnable)
      {
         return(mService.get().enableAutoAF(bEnable));
      }
      public boolean enableStereo(boolean bEnable)
      {
         return(mService.get().enableStereo(bEnable));
      }
      public boolean isAntennaAvailable()
      {
         return(mService.get().isAntennaAvailable());
      }
      public boolean isWiredHeadsetAvailable()
      {
         return(mService.get().isWiredHeadsetAvailable());
      }
      public boolean isCallActive()
      {
          return(mService.get().isCallActive());
      }
      public int getRssi()
      {
          return (mService.get().getRssi());
      }
      public int getIoC()
      {
          return (mService.get().getIoC());
      }
      public int getMpxDcc()
      {
          return (mService.get().getMpxDcc());
      }
      public int getIntDet()
      {
          return (mService.get().getIntDet());
      }
      public void setHiLoInj(int inj)
      {
          mService.get().setHiLoInj(inj);
      }
      public void delayedStop(long duration, int nType)
      {
          mService.get().delayedStop(duration, nType);
      }
      public void cancelDelayedStop(int nType)
      {
          mService.get().cancelDelayedStop(nType);
      }
      public void requestFocus()
      {
          mService.get().requestFocus();
      }
      public int getSINR()
      {
          return (mService.get().getSINR());
      }
      public boolean setSinrTh(int sinr)
      {
          return (mService.get().setSinrTh(sinr));
      }
      public int getSinrTh()
      {
          return (mService.get().getSinrTh());
      }
      public boolean isSleepTimerActive()
      {
           return (mService.get().isSleepTimerActive());
      }
      public boolean isPlayback() {
          return (mService.get().isPlayback());
      }
      public void playFM() {
          mService.get().playFM();
      }
      public void pauseFM() {
          mService.get().pauseFM();
      }

   }

   private final IBinder mBinder = new ServiceStub(this);

   private boolean setAudioPath(boolean analogMode) {

        if (mReceiver == null) {
              return false;
        }
        if (isAnalogModeEnabled() == analogMode) {
                Log.d(LOGTAG,"Analog Path already is set to "+analogMode);
                return false;
        }
        if (!isAnalogModeSupported()) {
                Log.d(LOGTAG,"Analog Path is not supported ");
                return false;
        }
        if (SystemProperties.getBoolean("hw.fm.digitalpath",false)) {
                return false;
        }

        boolean state = mReceiver.setAnalogMode(analogMode);
        if (false == state) {
            Log.d(LOGTAG, "Error in toggling analog/digital path " + analogMode);
            return false;
        }
        misAnalogPathEnabled = analogMode;
        return true;
   }
  /*
   * Turn ON FM: Powers up FM hardware, and initializes the FM module
   *                                                                                 .
   * @return true if fm Enable api was invoked successfully, false if the api failed.
   */
   private boolean fmOn() {
      boolean bStatus=false;
      int Defualt_SINRThreshold =7;
      mWakeLock.acquire(10*1000);
      if ( TelephonyManager.CALL_STATE_IDLE != getCallState() ) {
         return bStatus;
      }

      if(mReceiver == null)
      {
         try {
            mReceiver = new FmReceiver(FMRADIO_DEVICE_FD_STRING, fmCallbacks);
         }
         catch (InstantiationException e)
         {
            throw new RuntimeException("FmReceiver service not available!");
         }
      }

      if (mReceiver != null)
      {
         if (isFmOn())
         {
            /* FM Is already on,*/
            bStatus = true;
            Log.d(LOGTAG, "mReceiver.already enabled");
         }
         else
         {
            // This sets up the FM radio device
            FmConfig config = FmSharedPreferences.getFMConfiguration();
            Log.d(LOGTAG, "fmOn: RadioBand   :"+ config.getRadioBand());
            Log.d(LOGTAG, "fmOn: Emphasis    :"+ config.getEmphasis());
            Log.d(LOGTAG, "fmOn: ChSpacing   :"+ config.getChSpacing());
            Log.d(LOGTAG, "fmOn: RdsStd      :"+ config.getRdsStd());
            Log.d(LOGTAG, "fmOn: LowerLimit  :"+ config.getLowerLimit());
            Log.d(LOGTAG, "fmOn: UpperLimit  :"+ config.getUpperLimit());
	     if(config.getLowerLimit() == 91000) region = 1;
            bStatus = mReceiver.enable(FmSharedPreferences.getFMConfiguration(),getApplicationContext());
	     Defualt_SINRThreshold = mReceiver.getSINRThreshold();
	     Log.d(LOGTAG, "Get defualt Sinr Threshold: " + Defualt_SINRThreshold);
             //if (Defualt_SINRThreshold < 7)
	        // mReceiver.setSINRThreshold(7);
	     //Defualt_SINRThreshold = mReceiver.getSINRThreshold();
	     //Log.d(LOGTAG, "Get Sinr Threshold: " + Defualt_SINRThreshold);
            enableStereo(FmSharedPreferences.getAudioOutputMode());
            if (isSpeakerEnabled()) {
                setAudioPath(false);
            } else {
                setAudioPath(true);
            }
            Log.d(LOGTAG, "mReceiver.enable done, Status :" +  bStatus);
         }

         if (bStatus == true)
         {
            /* Put the hardware into normal mode */
            bStatus = setLowPowerMode(false);
            Log.d(LOGTAG, "setLowPowerMode done, Status :" +  bStatus);


            AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            if( (audioManager != null) &&(false == mPlaybackInProgress) )
            {
               Log.d(LOGTAG, "mAudioManager.setFmRadioOn = true \n" );
               //audioManager.setParameters("FMRadioOn="+mAudioDevice);
               int state =  getCallState();
               if ( TelephonyManager.CALL_STATE_IDLE != getCallState() )
               {
                 fmActionOnCallState(state);
               } else {
                   if(!(mResumeAfterCall && !resumePlayback)) {
                       // caoyang_20140722 Add: if volume >= 11, set volume to default volume index. @{
                   /*    if(Build.CUSTOM_NAME.contains("Lenovo")) {
                           if(!isSpeakerEnabled()) {
                               if(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) >= 11) {
                                   audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                                           audioManager.getDefaultStreamVolume(AudioManager.STREAM_MUSIC),
                                           0);
                               }
                           }
                       }*/     // fix by lihongya 20141229
                       // @}
                       //add by puqirui @2014.9.25  if volume >= 9, set volume to 4 
                       if(Build.CUSTOM_NAME.contains("Prestigio")) {
                           if(!isSpeakerEnabled()) {
                               if(audioManager.getStreamVolume(AudioManager.STREAM_MUSIC) >= 9) {
                                   audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                                           5,
                                           0);
                               }
                           }
                       }
                       startFM(); // enable FM Audio only when Call is IDLE
                   }
               }
               Log.d(LOGTAG, "mAudioManager.setFmRadioOn done \n" );
            }
            if (mReceiver != null) {
                bStatus = mReceiver.registerRdsGroupProcessing(FmReceiver.FM_RX_RDS_GRP_RT_EBL|
                                                           FmReceiver.FM_RX_RDS_GRP_PS_EBL|
                                                           FmReceiver.FM_RX_RDS_GRP_AF_EBL|
                                                           FmReceiver.FM_RX_RDS_GRP_PS_SIMPLE_EBL);
                Log.d(LOGTAG, "registerRdsGroupProcessing done, Status :" +  bStatus);
            }
            bStatus = enableAutoAF(FmSharedPreferences.getAutoAFSwitch());
            Log.d(LOGTAG, "enableAutoAF done, Status :" +  bStatus);

            /* There is no internal Antenna*/
            bStatus = mReceiver.setInternalAntenna(false);
            Log.d(LOGTAG, "setInternalAntenna done, Status :" +  bStatus);

            /* Read back to verify the internal Antenna mode*/
            readInternalAntennaAvailable();

            startNotification();
            mFMOn = true;
            bStatus = true;
         }
         else
         {
            mReceiver = null; // as enable failed no need to disable
                              // failure of enable can be because handle
                              // already open which gets effected if
                              // we disable
            stop();
         }
      }
      return(bStatus);
   }

  /*
   * Turn OFF FM Operations: This disables all the current FM operations             .
   */
   private void fmOperationsOff() {
      if (isFmRecordingOn())
      {
          stopRecording();
      }
      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      if(audioManager != null)
      {
         Log.d(LOGTAG, "audioManager.setFmRadioOn = false \n" );
         unMute();
         stopFM();
         //audioManager.setParameters("FMRadioOn=false");
         Log.d(LOGTAG, "audioManager.setFmRadioOn false done \n" );
      }

      if ( mSpeakerPhoneOn)
      {
          // caoyang_20131009 bug153416 Delete @{
          //mSpeakerPhoneOn = false;
          // @}
          AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_NONE);
      }
      sendRecordServiceIntent(RECORD_STOP);
      if (isAnalogModeEnabled()) {
              SystemProperties.set("hw.fm.isAnalog","false");
              misAnalogPathEnabled = false;
      }
   }

  /*
   * Reset (OFF) FM Operations: This resets all the current FM operations             .
   */
   private void fmOperationsReset() {
      if ( mSpeakerPhoneOn)
      {
          mSpeakerPhoneOn = false;
          mute();
          stopFM();
          setAudioPath(true);
          AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_NONE);
          unMute();
      }

      if (isFmRecordingOn())
      {
          stopRecording();
      }

      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      if(audioManager != null)
      {
         Log.d(LOGTAG, "audioManager.setFmRadioOn = false \n" );
         unMute();
         resetFM();
         //audioManager.setParameters("FMRadioOn=false");
         Log.d(LOGTAG, "audioManager.setFmRadioOn false done \n" );
      }

      if (isAnalogModeEnabled()) {
              SystemProperties.set("hw.fm.isAnalog","false");
              misAnalogPathEnabled = false;
      }
   }

  /*
   * Turn OFF FM: Disable the FM Host and hardware                                  .
   *                                                                                 .
   * @return true if fm Disable api was invoked successfully, false if the api failed.
   */
   private boolean fmOff() {
      boolean bStatus=false;

      fmOperationsOff();

      // This will disable the FM radio device
      if (mReceiver != null)
      {
         bStatus = mReceiver.disable();
         mReceiver = null;
      }
      if(!mResumeAfterCall){
          // caoyang_20140215 bug187849 Add @{
          stop();
          FMRadio.resetSleepAtPhoneTime();
          // @}
          Intent intent = new Intent(FMRadio.ACTION_EXIT_FM);
          sendBroadcast(intent);
      }
      return(bStatus);
   }

  /*
   * Turn OFF FM: Disable the FM Host when hardware resets asynchronously            .
   *                                                                                 .
   * @return true if fm Reset api was invoked successfully, false if the api failed  .
   */
   private boolean fmRadioReset() {
      boolean bStatus=false;

      Log.v(LOGTAG, "fmRadioReset");

      fmOperationsReset();

      // This will reset the FM radio receiver
      if (mReceiver != null)
      {
         bStatus = mReceiver.reset();
         mReceiver = null;
      }
      stop();
      return(bStatus);
   }

   /* Returns whether FM hardware is ON.
    *
    * @return true if FM was tuned, searching. (at the end of
    * the search FM goes back to tuned).
    *
    */
   public boolean isFmOn() {
      return mFMOn;
   }

   /* Returns true if Analog Path is enabled */
   public boolean isAnalogModeEnabled() {
         return misAnalogPathEnabled;
   }

   public boolean isAnalogModeSupported() {
        return misAnalogModeSupported;
   }

   public boolean isFmRecordingOn() {
      return mFmRecordingOn;
   }

   /* Return true if need to resume speaker */
   public boolean isResumeSpeaker(){
       return resumeSpeaker;
   }

   public boolean isSpeakerEnabled() {
      return mSpeakerPhoneOn;
   }
   public boolean isExternalStorageAvailable() {
     boolean mStorageAvail = false;
     String state = Environment.getExternalStorageState();

     if(Environment.MEDIA_MOUNTED.equals(state)){
         Log.d(LOGTAG, "device available");
         mStorageAvail = true;
     }
     return mStorageAvail;
   }
   public void enableSpeaker(boolean speakerOn) {
	Log.d(LOGTAG,"enableSpeaker speakerOn="+speakerOn);
       if(isCallActive())
           return ;
       mSpeakerPhoneOn = speakerOn;
       boolean analogmode = isAnalogModeSupported();
	Log.d(LOGTAG,"enableSpeaker analogmode="+analogmode);
       if (false == speakerOn) {
           if (analogmode) {
                if (isFmRecordingOn())
                    stopRecording();
                stopFM();
               AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_NONE);
               if (mMuted) {
                   setAudioPath(true);
               } else {
                   mute();
                   setAudioPath(true);
                   unMute();
               }
           } else {
               AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_NONE);
           }
           if (analogmode)
                startFM();


            // caoyang_20140722 Add: High volume toast. @{
            if(Build.CUSTOM_NAME.contains("Lenovo")) {
                if(isPlayback() && mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC) >= 11) {
                    Log.d(LOGTAG, "Speak off. Send volume toast message.");
                    mVolumeToastHandler.removeMessages(VOLUME_TOAST);
                    Message msg = mVolumeToastHandler.obtainMessage(VOLUME_TOAST);
                    mVolumeToastHandler.sendMessageDelayed(msg, VOLUME_TOAST_DELAYED);
                }
            }
            // @}
       }


       //Need to turn off BT path when Speaker is set on vice versa.
       if( !mA2dpDeviceSupportInHal && !analogmode && true == mA2dpDeviceState.isDeviceAvailable()) {
           if( ((true == mOverA2DP) && (true == speakerOn)) ||
               ((false == mOverA2DP) && (false == speakerOn)) ) {
              //disable A2DP playback for speaker option
               stopFM();
               startFM();
           }
       }
       if (speakerOn) {
           if (analogmode) {
                 stopFM();
                 if (mMuted) {
                     setAudioPath(false);
                 } else {
                     mute();
                     setAudioPath(false);
                     unMute();
                 }
           }
           AudioSystem.setForceUse(AudioSystem.FOR_MEDIA, AudioSystem.FORCE_SPEAKER);
           if (analogmode)
                startFM();

            // caoyang_20140722 Add: High volume toast. @{
            if(Build.CUSTOM_NAME.contains("Lenovo")) {
                Log.d(LOGTAG, "Speaker on. Remove volume toast message.");
                mVolumeToastHandler.removeMessages(VOLUME_TOAST);
            }
            // @}
       }

	AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
	//audioManager.setStreamMute(AudioManager.STREAM_FM,false);
	audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);

   }
  /*
   *  ReConfigure the FM Setup parameters
   *  - Band
   *  - Channel Spacing (50/100/200 KHz)
   *  - Emphasis (50/75)
   *  - Frequency limits
   *  - RDS/RBDS standard
   *
   * @return true if configure api was invoked successfully, false if the api failed.
   */
   public boolean fmReconfigure() {
      boolean bStatus=false;
      Log.d(LOGTAG, "fmReconfigure");
      if (mReceiver != null)
      {
         // This sets up the FM radio device
         FmConfig config = FmSharedPreferences.getFMConfiguration();
         Log.d(LOGTAG, "RadioBand   :"+ config.getRadioBand());
         Log.d(LOGTAG, "Emphasis    :"+ config.getEmphasis());
         Log.d(LOGTAG, "ChSpacing   :"+ config.getChSpacing());
         Log.d(LOGTAG, "RdsStd      :"+ config.getRdsStd());
         Log.d(LOGTAG, "LowerLimit  :"+ config.getLowerLimit());
         Log.d(LOGTAG, "UpperLimit  :"+ config.getUpperLimit());
	  if(config.getLowerLimit() == 91000)
	  region = 1;
         bStatus = mReceiver.configure(config);
      }
      return(bStatus);
   }

   /*
    * Register UI/Activity Callbacks
    */
   public void registerCallbacks(IFMRadioServiceCallbacks cb)
   {
      mCallbacks = cb;
   }

   /*
    *  unRegister UI/Activity Callbacks
    */
   public void unregisterCallbacks()
   {
      mCallbacks=null;
   }

   /*
   *  Route Audio to headset or speaker phone
   *  @return true if routeAudio call succeeded, false if the route call failed.
   */
   public boolean routeAudio(int audioDevice) {
      boolean bStatus=false;
      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

      //Log.d(LOGTAG, "routeAudio: " + audioDevice);

      switch (audioDevice) {

        case RADIO_AUDIO_DEVICE_WIRED_HEADSET:
            mAudioDevice = "headset";
            break;

        case RADIO_AUDIO_DEVICE_SPEAKER:
            mAudioDevice = "speaker";
            break;

        default:
            mAudioDevice = "headset";
            break;
      }

      if (mReceiver != null)
      {
      //audioManager.setParameters("FMRadioOn=false");
      //Log.d(LOGTAG, "mAudioManager.setFmRadioOn =" + mAudioDevice );
      //audioManager.setParameters("FMRadioOn="+mAudioDevice);
      //Log.d(LOGTAG, "mAudioManager.setFmRadioOn done \n");
       }

       return bStatus;
   }

  /*
   *  Mute FM Hardware (SoC)
   * @return true if set mute mode api was invoked successfully, false if the api failed.
   */
   public boolean mute() {
      boolean bCommandSent=true;
      if(isMuted())
          return bCommandSent;
      if(isCallActive())
         return false;
      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      Log.d(LOGTAG, "mute:");
      if (audioManager != null)
      {
         mMuted = true;
         //audioManager.setStreamMute(AudioManager.STREAM_FM,true);
         audioManager.setStreamMute(AudioManager.STREAM_MUSIC,true);
      }
      return bCommandSent;
   }

   /*
   *  UnMute FM Hardware (SoC)
   * @return true if set mute mode api was invoked successfully, false if the api failed.
   */
   public boolean unMute() {
      boolean bCommandSent=true;
      if(!isMuted())
          return bCommandSent;
      if(isCallActive())
         return false;
      Log.d(LOGTAG, "unMute:");
      AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
      if (audioManager != null)
      {
         mMuted = false;
         //audioManager.setStreamMute(AudioManager.STREAM_FM,false);
         audioManager.setStreamMute(AudioManager.STREAM_MUSIC,false);
         if (mResumeAfterCall)
         {
             //We are unmuting FM in a voice call. Need to enable FM audio routing.
             startFM();
         }
      }
      return bCommandSent;
   }

   /* Returns whether FM Hardware(Soc) Audio is Muted.
    *
    * @return true if FM Audio is muted, false if not muted.
    *
    */
   public boolean isMuted() {
      return mMuted;
   }

   public boolean isPlayback() {
       return mPlaybackInProgress;
   }

   public void playFM() {
       if (isFmOn()) {
           if(mPlaybackInProgress) {
               Log.d(LOGTAG, "FM is already playing.");
               return;
           }
           startFM();
           //start fix bug of 3590_b01-981 @20170601 by chenhaoran
           resumeSpeaker = false;
           //end
       }
       startNotification();
   }

   public void pauseFM() {
       if (isFmOn()) {
           if(!mPlaybackInProgress) {
               Log.d(LOGTAG, "FM is already paused.");
               return;
           }
           mResumeAfterCall = false;
           if (mSpeakerPhoneOn) {
               resumeSpeaker = true;
           }
           if(true == isFmRecordingOn()) {
               stopRecording();
           }
           stopFM();
       }
       startNotification();
   }

   /* Tunes to the specified frequency
    *
    * @return true if Tune command was invoked successfully, false if not muted.
    *  Note: Callback FmRxEvRadioTuneStatus will be called when the tune
    *        is complete
    */
   public boolean tune(int frequency) {
      boolean bCommandSent=false;
      double doubleFrequency = frequency/1000.00;

      Log.d(LOGTAG, "tuneRadio:  " + doubleFrequency);
      if (mReceiver != null)
      {
         mReceiver.setStation(frequency);
         bCommandSent = true;
      }
      return bCommandSent;
   }

   /* Seeks (Search for strong station) to the station in the direction specified
    * relative to the tuned station.
    * boolean up: true - Search in the forward direction.
    *             false - Search in the backward direction.
    * @return true if Seek command was invoked successfully, false if not muted.
    *  Note: 1. Callback FmRxEvSearchComplete will be called when the Search
    *        is complete
    *        2. Callback FmRxEvRadioTuneStatus will also be called when tuned to a station
    *        at the end of the Search or if the seach was cancelled.
    */
   public boolean seek(boolean up)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         if (up == true)
         {
            Log.d(LOGTAG, "seek:  Up");
            mReceiver.searchStations(FmReceiver.FM_RX_SRCH_MODE_SEEK,
                                             FmReceiver.FM_RX_DWELL_PERIOD_1S,
                                             FmReceiver.FM_RX_SEARCHDIR_UP);
         }
         else
         {
            Log.d(LOGTAG, "seek:  Down");
            mReceiver.searchStations(FmReceiver.FM_RX_SRCH_MODE_SEEK,
                                             FmReceiver.FM_RX_DWELL_PERIOD_1S,
                                             FmReceiver.FM_RX_SEARCHDIR_DOWN);
         }
         bCommandSent = true;
      }
      return bCommandSent;
   }

   /* Scan (Search for station with a "preview" of "n" seconds)
    * FM Stations. It always scans in the forward direction relative to the
    * current tuned station.
    * int pty: 0 or a reserved PTY value- Perform a "strong" station search of all stations.
    *          Valid/Known PTY - perform RDS Scan for that pty.
    *
    * @return true if Scan command was invoked successfully, false if not muted.
    *  Note: 1. Callback FmRxEvRadioTuneStatus will be called when tuned to various stations
    *           during the Scan.
    *        2. Callback FmRxEvSearchComplete will be called when the Search
    *        is complete
    *        3. Callback FmRxEvRadioTuneStatus will also be called when tuned to a station
    *        at the end of the Search or if the seach was cancelled.
    *
    */
   public boolean scan(int pty)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "scan:  PTY: " + pty);
         if(FmSharedPreferences.isRBDSStd())
         {
            /* RBDS : Validate PTY value?? */
            if( ((pty  > 0) && (pty  <= 23)) || ((pty  >= 29) && (pty  <= 31)) )
            {
               bCommandSent = mReceiver.searchStations(FmReceiver.FM_RX_SRCHRDS_MODE_SCAN_PTY,
                                                       FmReceiver.FM_RX_DWELL_PERIOD_2S,
                                                       FmReceiver.FM_RX_SEARCHDIR_UP,
                                                       pty,
                                                       0);
            }
            else
            {
               bCommandSent = mReceiver.searchStations(FmReceiver.FM_RX_SRCH_MODE_SCAN,
                                                FmReceiver.FM_RX_DWELL_PERIOD_1S,
                                                FmReceiver.FM_RX_SEARCHDIR_UP);
            }
         }
         else
         {
            /* RDS : Validate PTY value?? */
            if( (pty  > 0) && (pty  <= 31) )
            {
               bCommandSent = mReceiver.searchStations(FmReceiver.FM_RX_SRCHRDS_MODE_SCAN_PTY,
                                                          FmReceiver.FM_RX_DWELL_PERIOD_2S,
                                                          FmReceiver.FM_RX_SEARCHDIR_UP,
                                                          pty,
                                                          0);
            }
            else
            {
               bCommandSent = mReceiver.searchStations(FmReceiver.FM_RX_SRCH_MODE_SCAN,
                                                FmReceiver.FM_RX_DWELL_PERIOD_1S,
                                                FmReceiver.FM_RX_SEARCHDIR_UP);
            }
         }
      }
      return bCommandSent;
   }

   /* Search for the 'numStations' number of strong FM Stations.
    *
    * It searches in the forward direction relative to the current tuned station.
    * int numStations: maximum number of stations to search.
    *
    * @return true if Search command was invoked successfully, false if not muted.
    *  Note: 1. Callback FmRxEvSearchListComplete will be called when the Search
    *        is complete
    *        2. Callback FmRxEvRadioTuneStatus will also be called when tuned to
    *        the previously tuned station.
    */
   public boolean searchStrongStationList(int numStations)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "searchStrongStationList:  numStations: " + numStations);
         bCommandSent = mReceiver.searchStationList(FmReceiver.FM_RX_SRCHLIST_MODE_STRONG,
                                                    FmReceiver.FM_RX_SEARCHDIR_UP,
                                                    numStations,
                                                    0);
      }
      return bCommandSent;
   }

   /* Search for the FM Station that matches the RDS PI (Program Identifier) code.
    * It always scans in the forward direction relative to the current tuned station.
    * int piCode: PI Code of the station to search.
    *
    * @return true if Search command was invoked successfully, false if not muted.
    *  Note: 1. Callback FmRxEvSearchComplete will be called when the Search
    *        is complete
    *        2. Callback FmRxEvRadioTuneStatus will also be called when tuned to a station
    *        at the end of the Search or if the seach was cancelled.
    */
   public boolean seekPI(int piCode)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "seekPI:  piCode: " + piCode);
         bCommandSent = mReceiver.searchStations(FmReceiver.FM_RX_SRCHRDS_MODE_SEEK_PI,
                                                            FmReceiver.FM_RX_DWELL_PERIOD_1S,
                                                            FmReceiver.FM_RX_SEARCHDIR_UP,
                                                            0,
                                                            piCode
                                                            );
      }
      return bCommandSent;
   }


  /* Cancel any ongoing Search (Seek/Scan/SearchStationList).
   *
   * @return true if Search command was invoked successfully, false if not muted.
   *  Note: 1. Callback FmRxEvSearchComplete will be called when the Search
   *        is complete/cancelled.
   *        2. Callback FmRxEvRadioTuneStatus will also be called when tuned to a station
   *        at the end of the Search or if the seach was cancelled.
   */
   public boolean cancelSearch()
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "cancelSearch");
         bCommandSent = mReceiver.cancelSearch();
      }
      return bCommandSent;
   }

   /* Retrieves the RDS Program Service (PS) String.
    *
    * @return String - RDS PS String.
    *  Note: 1. This is a synchronous call that should typically called when
    *           Callback FmRxEvRdsPsInfo is invoked.
    *        2. Since PS contains multiple fields, this Service reads all the fields and "caches"
    *        the values and provides this helper routine for the Activity to get only the information it needs.
    *        3. The "cached" data fields are always "cleared" when the tune status changes.
    */
   public String getProgramService() {
      String str = "";
      if (mFMRxRDSData != null)
      {
         str = mFMRxRDSData.getPrgmServices();
         if(str == null)
         {
            str= "";
         }
      }
      Log.d(LOGTAG, "Program Service: [" + str + "]");
      return str;
   }

   /* Retrieves the RDS Radio Text (RT) String.
    *
    * @return String - RDS RT String.
    *  Note: 1. This is a synchronous call that should typically called when
    *           Callback FmRxEvRdsRtInfo is invoked.
    *        2. Since RT contains multiple fields, this Service reads all the fields and "caches"
    *        the values and provides this helper routine for the Activity to get only the information it needs.
    *        3. The "cached" data fields are always "cleared" when the tune status changes.
    */
   public String getRadioText() {
      String str = "";
      if (mFMRxRDSData != null)
      {
         str = mFMRxRDSData.getRadioText();
         if(str == null)
         {
            str= "";
         }
      }
      Log.d(LOGTAG, "Radio Text: [" + str + "]");
      return str;
   }

   /* Retrieves the RDS Program Type (PTY) code.
    *
    * @return int - RDS PTY code.
    *  Note: 1. This is a synchronous call that should typically called when
    *           Callback FmRxEvRdsRtInfo and or FmRxEvRdsPsInfo is invoked.
    *        2. Since RT/PS contains multiple fields, this Service reads all the fields and "caches"
    *        the values and provides this helper routine for the Activity to get only the information it needs.
    *        3. The "cached" data fields are always "cleared" when the tune status changes.
    */
   public int getProgramType() {
      int pty = -1;
      if (mFMRxRDSData != null)
      {
         pty = mFMRxRDSData.getPrgmType();
      }
      Log.d(LOGTAG, "PTY: [" + pty + "]");
      return pty;
   }

   /* Retrieves the RDS Program Identifier (PI).
    *
    * @return int - RDS PI code.
    *  Note: 1. This is a synchronous call that should typically called when
    *           Callback FmRxEvRdsRtInfo and or FmRxEvRdsPsInfo is invoked.
    *        2. Since RT/PS contains multiple fields, this Service reads all the fields and "caches"
    *        the values and provides this helper routine for the Activity to get only the information it needs.
    *        3. The "cached" data fields are always "cleared" when the tune status changes.
    */
   public int getProgramID() {
      int pi = -1;
      if (mFMRxRDSData != null)
      {
         pi = mFMRxRDSData.getPrgmId();
      }
      Log.d(LOGTAG, "PI: [" + pi + "]");
      return pi;
   }


   /* Retrieves the station list from the SearchStationlist.
    *
    * @return Array of integers that represents the station frequencies.
    * Note: 1. This is a synchronous call that should typically called when
    *           Callback onSearchListComplete.
    */
   public int[] getSearchList()
   {
      int[] frequencyList = null;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "getSearchList: ");
         frequencyList = mReceiver.getStationList();
      }
      return frequencyList;
   }

   /* Set the FM Power Mode on the FM hardware SoC.
    * Typically used when UI/Activity is in the background, so the Host is interrupted less often.
    *
    * boolean bLowPower: true: Enable Low Power mode on FM hardware.
    *                    false: Disable Low Power mode on FM hardware. (Put into normal power mode)
    * @return true if set power mode api was invoked successfully, false if the api failed.
    */
   public boolean setLowPowerMode(boolean bLowPower)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "setLowPowerMode: " + bLowPower);
         if(bLowPower)
         {
            bCommandSent = mReceiver.setPowerMode(FmReceiver.FM_RX_LOW_POWER_MODE);
         }
         else
         {
            bCommandSent = mReceiver.setPowerMode(FmReceiver.FM_RX_NORMAL_POWER_MODE);
         }
      }
      return bCommandSent;
   }

   /* Get the FM Power Mode on the FM hardware SoC.
    *
    * @return the device power mode.
    */
   public int getPowerMode()
   {
      int powerMode=FmReceiver.FM_RX_NORMAL_POWER_MODE;
      if (mReceiver != null)
      {
         powerMode = mReceiver.getPowerMode();
         Log.d(LOGTAG, "getLowPowerMode: " + powerMode);
      }
      return powerMode;
   }

  /* Set the FM module to auto switch to an Alternate Frequency for the
   * station if one the signal strength of that frequency is stronger than the
   * current tuned frequency.
   *
   * boolean bEnable: true: Auto switch to stronger alternate frequency.
   *                  false: Do not switch to alternate frequency.
   *
   * @return true if set Auto AF mode api was invoked successfully, false if the api failed.
   *  Note: Callback FmRxEvRadioTuneStatus will be called when tune
   *        is complete to a different frequency.
   */
   public boolean enableAutoAF(boolean bEnable)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "enableAutoAF: " + bEnable);
         bCommandSent = mReceiver.enableAFjump(bEnable);
      }
      return bCommandSent;
   }

   /* Set the FM module to Stereo Mode or always force it to Mono Mode.
    * Note: The stereo mode will be available only when the station is broadcasting
    * in Stereo mode.
    *
    * boolean bEnable: true: Enable Stereo Mode.
    *                  false: Always stay in Mono Mode.
    *
    * @return true if set Stereo mode api was invoked successfully, false if the api failed.
    */
   public boolean enableStereo(boolean bEnable)
   {
      boolean bCommandSent=false;
      if (mReceiver != null)
      {
         Log.d(LOGTAG, "enableStereo: " + bEnable);
         bCommandSent = mReceiver.setStereoMode(bEnable);
      }
      return bCommandSent;
   }

   /** Determines if an internal Antenna is available.
    *  Returns the cached value initialized on FMOn.
    *
    * @return true if internal antenna is available or wired
    *         headset is plugged in, false if internal antenna is
    *         not available and wired headset is not plugged in.
    */
   public boolean isAntennaAvailable()
   {
      boolean bAvailable = false;
      if ((mInternalAntennaAvailable) || (mHeadsetPlugged) )
      {
         bAvailable = true;
      }
      return bAvailable;
   }

   public static long getAvailableSpace() {
       String state = Environment.getExternalStorageState();
       Log.d(LOGTAG, "External storage state=" + state);
       if (Environment.MEDIA_CHECKING.equals(state)) {
           return PREPARING;
       }
       if (!Environment.MEDIA_MOUNTED.equals(state)) {
           return UNAVAILABLE;
       }

       try {
            File sampleDir = Environment.getExternalStorageDirectory();
            StatFs stat = new StatFs(sampleDir.getAbsolutePath());
            return stat.getAvailableBlocks() * (long) stat.getBlockSize();
       } catch (Exception e) {
            Log.i(LOGTAG, "Fail to access external storage", e);
       }
       return UNKNOWN_SIZE;
  }

   public long getSDCardAvailableSpace() {
       String state = getSDState(FMRadioService.this);
       Log.d(LOGTAG, "External storage state=" + state);
       if (!Environment.MEDIA_MOUNTED.equals(state)) {
           return UNAVAILABLE;
       }

       try {
            StatFs stat = new StatFs(getSDPath(FMRadioService.this));
            return stat.getAvailableBlocks() * (long) stat.getBlockSize();
       } catch (Exception e) {
            Log.i(LOGTAG, "Fail to access external storage", e);
       }
       return UNKNOWN_SIZE;
  }

   private boolean updateAndShowStorageHint() {
       mStorageSpace = getAvailableSpace();
       return showStorageHint();
   }

   private boolean updateAndShowSDCardStorageHint() {
       mStorageSpace = getSDCardAvailableSpace();
       return showStorageHint();
   }

   private boolean showStorageHint() {
       String errorMessage = null;
       if (mStorageSpace == UNAVAILABLE) {
           errorMessage = getString(R.string.no_storage);
       } else if (mStorageSpace == PREPARING) {
           errorMessage = getString(R.string.preparing_sd);
       } else if (mStorageSpace == UNKNOWN_SIZE) {
           errorMessage = getString(R.string.access_sd_fail);
       } else if (mStorageSpace < LOW_STORAGE_THRESHOLD) {
           errorMessage = getString(R.string.spaceIsLow_content);
       }

       if (errorMessage != null) {
           Toast.makeText(this, errorMessage,
                    Toast.LENGTH_LONG).show();
           return false;
       }
       return true;
   }

    static String getSDPath(Context context) {
        String sd = null;
        StorageManager mStorageManager = (StorageManager) context
                .getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = mStorageManager.getVolumeList();
        for (int i = 0; i < volumes.length; i++) {
	     if("/storage/sdcard0".equals(SystemProperties.get("persist.sys.emmc", "/storage/sdcard0"))) {
                if (volumes[i].isRemovable() && !volumes[i].isPrimary()) {
                    sd = volumes[i].getPath();
                }
            }else {
                if (volumes[i].isRemovable() && volumes[i].isPrimary()) {
                    sd = volumes[i].getPath();
                }
            }
        }
        return sd;
    }

    static String getSDState(Context context) {
        StorageManager mStorageManager = (StorageManager) context
                .getSystemService(Context.STORAGE_SERVICE);
        return mStorageManager.getVolumeState(getSDPath(context));
    }

   /** Determines if a Wired headset is plugged in. Returns the
    *  cached value initialized on broadcast receiver
    *  initialization.
    *
    * @return true if wired headset is plugged in, false if wired
    *         headset is not plugged in.
    */
   public boolean isWiredHeadsetAvailable()
   {
      return (mHeadsetPlugged);
   }
   public boolean isCallActive()
   {
       //Non-zero: Call state is RINGING or OFFHOOK on the available subscriptions
       //zero: Call state is IDLE on all the available subscriptions
       if(0 != getCallState()) return true;
       return false;
   }
   public int getCallState()
   {
       return mCallStatus;
   }

   public void clearStationInfo() {
       if(mFMRxRDSData != null) {
          mFMRxRDSData.setRadioText("");
          mFMRxRDSData.setPrgmId(0);
          mFMRxRDSData.setPrgmType(0);
          mFMRxRDSData.setPrgmServices("");
       }
   }

   /* Receiver callbacks back from the FM Stack */
   FmRxEvCallbacksAdaptor fmCallbacks = new FmRxEvCallbacksAdaptor()
   {
      public void FmRxEvEnableReceiver() {
         Log.d(LOGTAG, "FmRxEvEnableReceiver");
      }
      public void FmRxEvDisableReceiver()
      {
         mFMOn = false;
         Log.d(LOGTAG, "FmRxEvDisableReceiver");
         mFMOn = false;
      }
      public void FmRxEvRadioReset()
      {
         Log.d(LOGTAG, "FmRxEvRadioReset");
         if(isFmOn()) {
             // Received radio reset event while FM is ON
             Log.d(LOGTAG, "FM Radio reset");
             fmRadioReset();
             try
             {
                /* Notify the UI/Activity, only if the service is "bound"
                   by an activity and if Callbacks are registered
                */
                if((mServiceInUse) && (mCallbacks != null) )
                {
                    mCallbacks.onRadioReset();
                }
             }
             catch (RemoteException e)
             {
                e.printStackTrace();
             }
         }
      }
      public void FmRxEvConfigReceiver()
      {
         Log.d(LOGTAG, "FmRxEvConfigReceiver");
      }
      public void FmRxEvMuteModeSet()
      {
         Log.d(LOGTAG, "FmRxEvMuteModeSet");
      }
      public void FmRxEvStereoModeSet()
      {
         Log.d(LOGTAG, "FmRxEvStereoModeSet");
      }
      public void FmRxEvRadioStationSet()
      {
         Log.d(LOGTAG, "FmRxEvRadioStationSet");
      }
      public void FmRxEvPowerModeSet()
      {
         Log.d(LOGTAG, "FmRxEvPowerModeSet");
      }
      public void FmRxEvSetSignalThreshold()
      {
         Log.d(LOGTAG, "FmRxEvSetSignalThreshold");
      }

      public void FmRxEvRadioTuneStatus(int frequency)
      {
         Log.d(LOGTAG, "FmRxEvRadioTuneStatus: Tuned Frequency: " +frequency);
         try
         {
            if(frequency==91000 && region == 1){
            	  region = 0;
                frequency = 91100;
            }
            FmSharedPreferences.setTunedFrequency(frequency);
            mPrefs.Save();
            //Log.d(LOGTAG, "Call mCallbacks.onTuneStatusChanged");
            /* Since the Tuned Status changed, clear out the RDSData cached */
            if(mReceiver != null) {
               clearStationInfo();
            }
            mFMRxRDSData = null;
            if(mCallbacks != null)
            {
               mCallbacks.onTuneStatusChanged(frequency);
            }
            /* Update the frequency in the StatusBar's Notification */
            startNotification();

         }
         catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }

      public void FmRxEvStationParameters()
      {
         Log.d(LOGTAG, "FmRxEvStationParameters");
      }

      public void FmRxEvRdsLockStatus(boolean bRDSSupported)
      {
         Log.d(LOGTAG, "FmRxEvRdsLockStatus: " + bRDSSupported);
         try
         {
            if(mCallbacks != null)
            {
               mCallbacks.onStationRDSSupported(bRDSSupported);
            }
         }
         catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }

      public void FmRxEvStereoStatus(boolean stereo)
      {
         Log.d(LOGTAG, "FmRxEvStereoStatus: " + stereo);
         try
         {
            if(mCallbacks != null)
            {
               mCallbacks.onAudioUpdate(stereo);
            }
         }
         catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }
      public void FmRxEvServiceAvailable(boolean signal)
      {
         Log.d(LOGTAG, "FmRxEvServiceAvailable");
         if(signal) {
             Log.d(LOGTAG, "FmRxEvServiceAvailable: Tuned frequency is above signal threshold level");
         }
         else {
             Log.d(LOGTAG, "FmRxEvServiceAvailable: Tuned frequency is below signal threshold level");
         }
      }
      public void FmRxEvGetSignalThreshold()
      {
         Log.d(LOGTAG, "FmRxEvGetSignalThreshold");
      }
      public void FmRxEvSearchInProgress()
      {
         Log.d(LOGTAG, "FmRxEvSearchInProgress");
      }
      public void FmRxEvSearchRdsInProgress()
      {
         Log.d(LOGTAG, "FmRxEvSearchRdsInProgress");
      }
      public void FmRxEvSearchListInProgress()
      {
         Log.d(LOGTAG, "FmRxEvSearchListInProgress");
      }
      public void FmRxEvSearchComplete(int frequency)
       {
         Log.d(LOGTAG, "FmRxEvSearchComplete: Tuned Frequency: " +frequency);
         try
         {
            FmSharedPreferences.setTunedFrequency(frequency);
            //Log.d(LOGTAG, "Call mCallbacks.onSearchComplete");
            /* Since the Tuned Status changed, clear out the RDSData cached */
            mFMRxRDSData = null;
            if(mCallbacks != null)
            {
               mCallbacks.onSearchComplete();
            }
            /* Update the frequency in the StatusBar's Notification */
            startNotification();
         }
         catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }

      public void FmRxEvSearchRdsComplete()
      {
         Log.d(LOGTAG, "FmRxEvSearchRdsComplete");
      }

      public void FmRxEvSearchListComplete()
      {
         Log.d(LOGTAG, "FmRxEvSearchListComplete");
         try
         {
            if(mCallbacks != null)
            {
               mCallbacks.onSearchListComplete();
            }
         } catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }

      public void FmRxEvSearchCancelled()
      {
         Log.d(LOGTAG, "FmRxEvSearchCancelled: Cancelled the on-going search operation.");
      }
      public void FmRxEvRdsGroupData()
      {
         Log.d(LOGTAG, "FmRxEvRdsGroupData");
      }

      public void FmRxEvRdsPsInfo() {
         Log.d(LOGTAG, "FmRxEvRdsPsInfo: ");
         try
         {
            if(mReceiver != null)
            {
               mFMRxRDSData = mReceiver.getPSInfo();
               if(mFMRxRDSData != null)
               {
                  Log.d(LOGTAG, "PI: [" + mFMRxRDSData.getPrgmId() + "]");
                  Log.d(LOGTAG, "PTY: [" + mFMRxRDSData.getPrgmType() + "]");
                  Log.d(LOGTAG, "PS: [" + mFMRxRDSData.getPrgmServices() + "]");
               }
               if(mCallbacks != null)
               {
                  mCallbacks.onProgramServiceChanged();
               }
            }
         } catch (RemoteException e)
         {
            e.printStackTrace();
         }
      }

      public void FmRxEvRdsRtInfo() {
         Log.d(LOGTAG, "FmRxEvRdsRtInfo");
         try
         {
            //Log.d(LOGTAG, "Call mCallbacks.onRadioTextChanged");
            if(mReceiver != null)
            {
               mFMRxRDSData = mReceiver.getRTInfo();
               if(mFMRxRDSData != null)
               {
                  Log.d(LOGTAG, "PI: [" + mFMRxRDSData.getPrgmId() + "]");
                  Log.d(LOGTAG, "PTY: [" + mFMRxRDSData.getPrgmType() + "]");
                  Log.d(LOGTAG, "RT: [" + mFMRxRDSData.getRadioText() + "]");
               }
               if(mCallbacks != null)
               {
                  mCallbacks.onRadioTextChanged();
               }
            }
         } catch (RemoteException e)
         {
            e.printStackTrace();
         }

      }

      public void FmRxEvRdsAfInfo()
      {
         Log.d(LOGTAG, "FmRxEvRdsAfInfo");
      }
      public void FmRxEvRdsPiMatchAvailable()
      {
         Log.d(LOGTAG, "FmRxEvRdsPiMatchAvailable");
      }
      public void FmRxEvRdsGroupOptionsSet()
      {
         Log.d(LOGTAG, "FmRxEvRdsGroupOptionsSet");
      }
      public void FmRxEvRdsProcRegDone()
      {
         Log.d(LOGTAG, "FmRxEvRdsProcRegDone");
      }
      public void FmRxEvRdsPiMatchRegDone()
      {
         Log.d(LOGTAG, "FmRxEvRdsPiMatchRegDone");
      }
   };


   /*
    *  Read the Tuned Frequency from the FM module.
    */
   private String getTunedFrequencyString() {

      double frequency = FmSharedPreferences.getTunedFrequency() / 1000.0;
      //add by wangpeng_20170411
       String frequencyString=null;
       Locale l = Locale.getDefault();
       String language =  l.getLanguage();
      if(language.contains("ru")) {
          frequencyString = frequency + " МГц";
      }else {
          frequencyString = frequency + " MHz";
      }
      return frequencyString;
       //end by wangpeng_20170411
   }
   public int getRssi() {
      if (mReceiver != null)
          return mReceiver.getRssi();
      else
          return Integer.MAX_VALUE;
   }
   public int getIoC() {
      if (mReceiver != null)
          return mReceiver.getIoverc();
      else
          return Integer.MAX_VALUE;
   }
   public int getIntDet() {
      if (mReceiver != null)
          return mReceiver.getIntDet();
      else
          return Integer.MAX_VALUE;
   }
   public int getMpxDcc() {
      if (mReceiver != null)
          return mReceiver.getMpxDcc();
      else
          return Integer.MAX_VALUE;
   }
   public void setHiLoInj(int inj) {
      if (mReceiver != null)
          mReceiver.setHiLoInj(inj);
   }
   public int getSINR() {
      if (mReceiver != null)
          return mReceiver.getSINR();
      else
          return Integer.MAX_VALUE;
   }
   public boolean setSinrSamplesCnt(int samplesCnt) {
      if(mReceiver != null)
         return mReceiver.setSINRsamples(samplesCnt);
      else
         return false;
   }
   public boolean setSinrTh(int sinr) {
      if(mReceiver != null)
         return mReceiver.setSINRThreshold(sinr);
      else
         return false;
   }
   public boolean setIntfDetLowTh(int intfLowTh) {
      if(mReceiver != null)
         return mReceiver.setOnChannelThreshold(intfLowTh);
      else
         return false;
   }
   public boolean setIntfDetHighTh(int intfHighTh) {
      if(mReceiver != null)
         return mReceiver.setOffChannelThreshold(intfHighTh);
      else
         return false;
   }
   public int getSearchAlgoType() {
       if(mReceiver != null)
          return mReceiver.getSearchAlgoType();
       else
          return -1;
   }

   public boolean isSleepTimerActive() {
      return mSleepActive;
   }
   public boolean setSearchAlgoType(int searchType) {
        if(mReceiver != null)
           return mReceiver.setSearchAlgoType(searchType);
        else
           return false;
   }
   public int getSinrFirstStage() {
        if(mReceiver != null)
           return mReceiver.getSinrFirstStage();
        else
           return Integer.MAX_VALUE;
   }
   public boolean setSinrFirstStage(int sinr) {
        if(mReceiver != null)
           return mReceiver.setSinrFirstStage(sinr);
        else
           return false;
   }
   public int getRmssiFirstStage() {
        if(mReceiver != null)
           return mReceiver.getRmssiFirstStage();
        else
           return Integer.MAX_VALUE;
   }
   public boolean setRmssiFirstStage(int rmssi) {
         if(mReceiver != null)
            return mReceiver.setRmssiFirstStage(rmssi);
         else
            return false;
   }
   public int getCFOMeanTh() {
          if(mReceiver != null)
             return mReceiver.getCFOMeanTh();
          else
             return Integer.MAX_VALUE;
   }
   public boolean setCFOMeanTh(int th) {
          if(mReceiver != null)
             return mReceiver.setCFOMeanTh(th);
          else
             return false;
   }
   public int getSinrSamplesCnt() {
          if(mReceiver != null)
             return mReceiver.getSINRsamples();
          else
             return Integer.MAX_VALUE;
   }
   public int getSinrTh() {
          if(mReceiver != null)
             return mReceiver.getSINRThreshold();
          else
             return Integer.MAX_VALUE;
   }

   boolean setAfJmpRmssiTh(int afJmpRmssiTh) {
          if(mReceiver != null)
             return mReceiver.setAFJumpRmssiTh(afJmpRmssiTh);
          else
             return false;
   }
   boolean setGoodChRmssiTh(int gdChRmssiTh) {
          if(mReceiver != null)
             return mReceiver.setGdChRmssiTh(gdChRmssiTh);
          else
             return false;
   }
   boolean setAfJmpRmssiSamplesCnt(int afJmpRmssiSmplsCnt) {
          if(mReceiver != null)
             return mReceiver.setAFJumpRmssiSamples(afJmpRmssiSmplsCnt);
          else
             return false;
   }
   int getAfJmpRmssiTh() {
          if(mReceiver != null)
             return mReceiver.getAFJumpRmssiTh();
          else
             return Integer.MIN_VALUE;
   }
   int getGoodChRmssiTh() {
          if(mReceiver != null)
             return mReceiver.getGdChRmssiTh();
          else
             return Integer.MAX_VALUE;
   }
   int getAfJmpRmssiSamplesCnt() {
          if(mReceiver != null)
             return mReceiver.getAFJumpRmssiSamples();
          else
             return Integer.MIN_VALUE;
   }
   private void setAlarmSleepExpired (long duration) {
       Intent i = new Intent(SLEEP_EXPIRED_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       Log.d(LOGTAG,"delayedStop called"+SystemClock.elapsedRealtime() + duration);
       am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + duration, pi);
       mSleepActive = true;
   }

   private void cancelAlarmSleepExpired(){
       Intent i = new Intent(SLEEP_EXPIRED_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       am.cancel(pi);
       mSleepActive = false;
   }

   private void setAlarmRecordTimeout(long duration){
       Intent i = new Intent(RECORD_EXPIRED_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       Log.d(LOGTAG,"delayedStop called"+SystemClock.elapsedRealtime() + duration);
       am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + duration, pi);
   }

   private void cancelAlarmRecordTimeout(){
       Intent i = new Intent(RECORD_EXPIRED_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       am.cancel(pi);
   }

   private void setAlarmDelayedServiceStop(){
       Intent i = new Intent(SERVICE_DELAYED_STOP_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       am.set(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + IDLE_DELAY, pi);
   }

   private void cancelAlarmDelayedServiceStop(){
       Intent i = new Intent(SERVICE_DELAYED_STOP_ACTION);
       AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
       PendingIntent pi = PendingIntent.getBroadcast(this, 0, i, 0);
       am.cancel(pi);
   }

   private void cancelAlarms(){
       cancelAlarmSleepExpired();
       cancelAlarmRecordTimeout();
       cancelAlarmDelayedServiceStop();
   }
   //handling the sleep and record stop when FM App not in focus
   private void delayedStop(long duration, int nType) {
       int whatId = (nType == STOP_SERVICE) ? STOPSERVICE_ONSLEEP: STOPRECORD_ONTIMEOUT ;
       if(nType == STOP_SERVICE)
           setAlarmSleepExpired(duration);
       else
           setAlarmRecordTimeout(duration);
   }
   private void cancelDelayedStop(int nType) {
       int whatId = (nType == STOP_SERVICE) ? STOPSERVICE_ONSLEEP: STOPRECORD_ONTIMEOUT ;
       if(nType == STOP_SERVICE)
           cancelAlarmSleepExpired();
       else
           cancelAlarmRecordTimeout();
   }
   private void requestFocus() {
      if( (false == mPlaybackInProgress) &&
          (true  == mStoppedOnFocusLoss) ) {
           // adding code for audio focus gain.
           AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
           audioManager.requestAudioFocus(mAudioFocusListener, AudioManager.STREAM_MUSIC,
                  AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);
           startFM();
           mStoppedOnFocusLoss = false;
       }
   }
   private OnAudioFocusChangeListener mAudioFocusListener = new OnAudioFocusChangeListener() {
       public void onAudioFocusChange(int focusChange) {
           mDelayedStopHandler.obtainMessage(FOCUSCHANGE, focusChange, 0).sendToTarget();
       }
   };
   //renli add 20160525 
   private BroadcastReceiver fmReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String cmd = intent.getStringExtra("command");
        if (isFmOn()){
				if(CMDPREVIOUS.equals(cmd)){
					seek(false);
				}else if(CMDNEXT.equals(cmd)){
					seek(true);}
			}
		}
	};
}
